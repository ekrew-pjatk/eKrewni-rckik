package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.naming.NameNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.io.entity.AddressEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.repository.AddressRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.AddressDto;

class AddressServiceImplTest {

	@InjectMocks
	AddressServiceImpl addressService;
	
	@Mock
	CenterRepository centerRepository;

	@Mock
	AddressRepository addressRepository;
	
	@Mock
	RandomStringGenerator randomStringGenerator;
	
	String centerId = "jciaugfabvaivgfisf";
	String addressId = "kjxhSIvhif78ejkiu9";
	
	CenterEntity centerEntity;
	AddressEntity addressEntity;
	
	@BeforeEach
	void setUp() throws Exception {
		
        MockitoAnnotations.initMocks(this);
		
        addressEntity = new AddressEntity();
        addressEntity.setId(1L);
        addressEntity.setAddressId(addressId);
        addressEntity.setCity("Tczew");
        addressEntity.setPostalCode("83-110");
        addressEntity.setStreet("Kasztanowa 25a");
        addressEntity.setLatitude("123456789");
        addressEntity.setLongitude("987654321");
        
		centerEntity = new CenterEntity();
		centerEntity.setId(1L);
		centerEntity.setName("RCKiK");
		centerEntity.setCenterId(centerId);
		centerEntity.setEmail("rckik@gov.pl");
		centerEntity.setAddress(addressEntity);
	}

	@Test
	final void testGetAddressByCenterId() throws NameNotFoundException {
		
        when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		when(addressRepository.findByCenter(any(CenterEntity.class))).thenReturn(addressEntity);
        
		AddressDto addressDto = addressService.getAddressByCenterId(centerId);
		
		assertNotNull(addressDto);
		assertEquals("Tczew", addressDto.getCity());
	}
	
	@Test
	final void testGetAddressByCenterId_UsernameNotFoundException() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		when(addressRepository.findByCenter(any(CenterEntity.class))).thenReturn(null);
		
		assertThrows(UsernameNotFoundException.class, () -> {
		 
			addressService.getAddressByCenterId(centerId);
		});
	}
	
	@Test
	final void testUpdateAddress() throws NameNotFoundException {
		
		when(addressRepository.findByAddressId(anyString())).thenReturn(addressEntity);
		
		when(addressRepository.save(any(AddressEntity.class))).thenReturn(addressEntity);
		
		AddressDto addressDto = new AddressDto();
		addressDto.setCity("Gdańsk");
		addressDto.setPostalCode("43-786");
		addressDto.setStreet("Jaworowa 15b");
		addressDto.setLatitude("657378536");
		addressDto.setLongitude("764733058");
		
		AddressDto updatedAddressDto = addressService.updateAddress(addressId, addressDto);
		assertEquals(addressDto.getCity(), updatedAddressDto.getCity());
		assertEquals(addressDto.getPostalCode(), updatedAddressDto.getPostalCode());
		assertEquals(addressDto.getStreet(), updatedAddressDto.getStreet());
		assertEquals(addressDto.getLatitude(), updatedAddressDto.getLatitude());
		assertEquals(addressDto.getLongitude(), updatedAddressDto.getLongitude());
		assertNotNull(updatedAddressDto.getAddressId());
		verify(addressRepository, times(1)).save(any(AddressEntity.class));
	}
}