package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.BloodResourceRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.BloodResourceDto;

class BloodResourceServiceImplTest {

	@InjectMocks
	BloodResourceServiceImpl bloodResourceService;

	@Mock
	BloodResourceRepository bloodResourceRepository;

	@Mock
	CenterUserRepository centerUserRepository;
	
	@Mock
	RandomStringGenerator randomStringGenerator;

	String bloodResourceId = "fjw85gh824uh5rn24";

	BloodResourceEntity bloodResourceEntity;
	CenterUserEntity centerUserEntity;
	CenterEntity centerEntity;
	
	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		bloodResourceEntity = new BloodResourceEntity();
		bloodResourceEntity.setId(1L);
		bloodResourceEntity.setBloodResourceId(bloodResourceId);
		bloodResourceEntity.setVolume(25.0);
		
		centerUserEntity = new CenterUserEntity();
		centerUserEntity.setId(1L);
		centerUserEntity.setUserId("g34y2987g");
		centerUserEntity.setLogin("login");
	}

	@Test
	final void testGetBloodResourceByBloodResourceId() {

		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(bloodResourceEntity);

		BloodResourceDto bloodResourceDto = bloodResourceService.getBloodResourceByBloodResourceId(bloodResourceId);

		assertNotNull(bloodResourceDto);
		assertEquals(25.0, bloodResourceDto.getVolume());
	}

	@Test
	final void testGetBloodResourceByBloodResourceId_UsernameNotFoundException() {

		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(null);

		assertThrows(UsernameNotFoundException.class, () -> {

			bloodResourceService.getBloodResourceByBloodResourceId(bloodResourceId);
		});
	}

	@Test
	final void testCreateBloodResource() {

		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(null);
		when(randomStringGenerator.generateBloodResourceId(anyInt())).thenReturn(bloodResourceId);

		when(bloodResourceRepository.save(any(BloodResourceEntity.class))).thenReturn(bloodResourceEntity);

		BloodResourceDto bloodResourceDto = new BloodResourceDto();
		bloodResourceDto.setVolume(25.0);

		BloodResourceDto storedBloodResource = bloodResourceService.createBloodResource(bloodResourceDto);
		assertNotNull(storedBloodResource);
		assertEquals(bloodResourceEntity.getVolume(), storedBloodResource.getVolume());
		assertNotNull(storedBloodResource.getBloodResourceId());
		verify(randomStringGenerator, times(1)).generateBloodResourceId(30);
		verify(bloodResourceRepository, times(1)).save(any(BloodResourceEntity.class));
	}

	@Test
	final void testCreateBloodResource_RuntimeException() {

		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(bloodResourceEntity);

		BloodResourceDto bloodResourceDto = new BloodResourceDto();
		bloodResourceDto.setBloodResourceId(bloodResourceId);

		assertThrows(RuntimeException.class, () -> {

			bloodResourceService.createBloodResource(bloodResourceDto);
		});
	}
	
	@Test
	final void testDeleteBloodResource() throws Exception {
		
		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(bloodResourceEntity);
		
		bloodResourceService.deleteBloodResource(bloodResourceId);
	
		verify(bloodResourceRepository, times(1)).delete(any(BloodResourceEntity.class));
	}
	
	@Test
	final void testDeleteBloodResource_Exeption() {
		
		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(null);
		
		assertThrows(Exception.class, () -> {
			
			bloodResourceService.deleteBloodResource(bloodResourceId);
		});
	}
	
	@Test
	final void testUpdateBloodResource() throws Exception {
		
		when(bloodResourceRepository.findByBloodResourceId(anyString())).thenReturn(bloodResourceEntity);
		when(centerUserRepository.findByLogin(anyString())).thenReturn(centerUserEntity);
		
		when(bloodResourceRepository.save(any(BloodResourceEntity.class))).thenReturn(bloodResourceEntity);
		
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		when(securityContext.getAuthentication().getName()).thenReturn("login");
		SecurityContextHolder.setContext(securityContext);
		
		
		CenterEntity centerEntity = new CenterEntity();
		centerEntity.setCenterId("nfdji986");
		bloodResourceEntity.setCenter(centerEntity);
		centerUserEntity.setCenter(centerEntity);
		centerUserEntity.setLogin("login");
		
		BloodResourceDto bloodResourceDto = new BloodResourceDto();
		bloodResourceDto.setVolume(50.0);
		
		BloodResourceDto updatedBloodResourceDto = bloodResourceService.updateBloodResource(bloodResourceId, bloodResourceDto);
		assertEquals(bloodResourceDto.getVolume(), updatedBloodResourceDto.getVolume());
		assertNotNull(updatedBloodResourceDto.getBloodResourceId());
		verify(bloodResourceRepository, times(1)).save(any(BloodResourceEntity.class));
	}
}