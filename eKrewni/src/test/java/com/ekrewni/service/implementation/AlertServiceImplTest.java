package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.naming.NameNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.io.entity.AlertEntity;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.AlertRepository;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.shared.dto.BloodDto;

class AlertServiceImplTest {

	@InjectMocks
	AlertServiceImpl alertService;

	@Mock
	AlertRepository alertRepository;

	@Mock
	CenterUserRepository centerUserRepository;

	@Mock
	BloodRepository bloodRepository;

	@Mock
	RandomStringGenerator randomStringGenerator;

	String alertId = "jvbbiutqh4";
	String bloodId = "fgdtmuhmtj";

	AlertEntity alertEntity;
	CenterUserEntity centerUserEntity;
	BloodEntity bloodEntity;

	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		alertEntity = new AlertEntity();
		alertEntity.setId(1L);
		alertEntity.setAlertId(alertId);
		alertEntity.setMessage("Mało!!!");
		alertEntity.setBlood(getBloodEntity());
		
		centerUserEntity = new CenterUserEntity();
		centerUserEntity.setId(1L);
		centerUserEntity.setUserId("g34y2987g");
		centerUserEntity.setLogin("login");
	}

	@Test
	final void testGetAlertByAlertId() {

		when(alertRepository.findByAlertId(anyString())).thenReturn(alertEntity);

		AlertDto alertDto = alertService.getAlertByAlertId(alertId);

		assertNotNull(alertDto);
		assertEquals("Mało!!!", alertDto.getMessage());
	}

	@Test
	final void testGetAlertByAlertId_UsernameNotFoundException() {

		when(alertRepository.findByAlertId(anyString())).thenReturn(null);

		assertThrows(UsernameNotFoundException.class, () -> {

			alertService.getAlertByAlertId(alertId);
		});
	}

	@Test
	final void testCreateAlert() {

		when(alertRepository.findByAlertId(anyString())).thenReturn(null);
		when(randomStringGenerator.generateAlertId(anyInt())).thenReturn(alertId);
		when(centerUserRepository.findByLogin(anyString())).thenReturn(centerUserEntity);
		when(bloodRepository.findByBloodTypeAndRh(anyString(), anyString())).thenReturn(bloodEntity);

		when(alertRepository.save(any(AlertEntity.class))).thenReturn(alertEntity);

		AlertDto alertDto = new AlertDto();
		alertDto.setMessage("Mało!!!");
		alertDto.setBlood(getBloodDto());

		AlertDto storedAlertDetails = alertService.createAlert(alertDto);
		assertNotNull(storedAlertDetails);
		assertEquals(alertEntity.getMessage(), storedAlertDetails.getMessage());
		assertNotNull(storedAlertDetails.getAlertId());
		verify(randomStringGenerator, times(1)).generateAlertId(30);
		verify(alertRepository, times(1)).save(any(AlertEntity.class));
	}

	@Test
	final void testCreateAlert_RuntimeException() {

		when(alertRepository.findByAlertId(anyString())).thenReturn(alertEntity);

		AlertDto alertDto = new AlertDto();
		alertDto.setAlertId(alertId);

		assertThrows(RuntimeException.class, () -> {

			alertService.createAlert(alertDto);
		});
	}

	@Test
	final void testDeleteAlert() throws Exception {
		
		when(alertRepository.findByAlertId(anyString())).thenReturn(alertEntity);
		
		alertService.deleteAlert(alertId);
	
		verify(alertRepository, times(1)).delete(any(AlertEntity.class));
	}
	
	@Test
	final void testDeleteAlert_Exeption() {
		
		when(alertRepository.findByAlertId(anyString())).thenReturn(null);
		
		assertThrows(Exception.class, () -> {
			
			alertService.deleteAlert(alertId);
		});
	}
	
	@Test
	final void testUpdateAlert() throws NameNotFoundException {
		
		when(alertRepository.findByAlertId(anyString())).thenReturn(alertEntity);
		when(centerUserRepository.findByLogin(anyString())).thenReturn(centerUserEntity);
		
		when(alertRepository.save(any(AlertEntity.class))).thenReturn(alertEntity);
		
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		when(securityContext.getAuthentication().getName()).thenReturn("login");
		SecurityContextHolder.setContext(securityContext);
		
		alertEntity.setCenterUser(centerUserEntity);
		
		AlertDto alertDto = new AlertDto();
		alertDto.setMessage("Bardzo mało!!!");
		
		AlertDto updatedAlertDto = alertService.updateAlert(alertId, alertDto);
		assertEquals(alertDto.getMessage(), updatedAlertDto.getMessage());
		assertNotNull(updatedAlertDto.getAlertId());
		verify(alertRepository, times(1)).save(any(AlertEntity.class));
	}
	
	private BloodDto getBloodDto() {

		BloodDto bloodDto = new BloodDto();
		bloodDto.setBloodType("AB");
		bloodDto.setRh("-");

		return bloodDto;
	}

	private BloodEntity getBloodEntity() {

		BloodDto bloodDto = getBloodDto();

		return new ModelMapper().map(bloodDto, BloodEntity.class);
	}
}