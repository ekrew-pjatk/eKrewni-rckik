package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.naming.NameNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.io.entity.CenterMessageEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.CenterMessageRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.CenterMessageDto;

class CenterMessageServiceImplTest {

	@InjectMocks
	CenterMessageServiceImpl centerMessageService;

	@Mock
	CenterMessageRepository centerMessageRepository;

	@Mock
	CenterUserRepository centerUserRepository;

	@Mock
	RandomStringGenerator randomStringGenerator;

	String messageId = "gr58nv85ty0";

	CenterMessageEntity centerMessageEntity;
	CenterUserEntity centerUserEntity;

	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		centerMessageEntity = new CenterMessageEntity();
		centerMessageEntity.setId(1L);
		centerMessageEntity.setCenterMessageId(messageId);
		centerMessageEntity.setText("Cześć!");
	}

	@Test
	final void testGetCenterMessageByMessageId() {

		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(centerMessageEntity);

		CenterMessageDto centerMessageDto = centerMessageService.getCenterMessageByMessageId(messageId);

		assertNotNull(centerMessageDto);
		assertEquals("Cześć!", centerMessageDto.getText());
	}

	@Test
	final void testGetCenterMessageByMessageId_UsernameNotFoundException() {

		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(null);

		assertThrows(UsernameNotFoundException.class, () -> {

			centerMessageService.getCenterMessageByMessageId(messageId);
		});
	}

	@Test
	final void testCreateMessage() {

		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(null);
		when(randomStringGenerator.generateCenterMessageId(anyInt())).thenReturn(messageId);
		when(centerUserRepository.findByLogin(anyString())).thenReturn(centerUserEntity);

		when(centerMessageRepository.save(any(CenterMessageEntity.class))).thenReturn(centerMessageEntity);

		CenterMessageDto centerMessageDto = new CenterMessageDto();
		centerMessageDto.setText("Cześć!");

		CenterMessageDto storedCenterMessage = centerMessageService.createMessage(centerMessageDto);
		assertNotNull(storedCenterMessage);
		assertEquals(centerMessageEntity.getText(), storedCenterMessage.getText());
		assertNotNull(storedCenterMessage.getCenterMessageId());
		verify(randomStringGenerator, times(1)).generateCenterMessageId(30);
		verify(centerMessageRepository, times(1)).save(any(CenterMessageEntity.class));
	}

	@Test
	final void testCreateMessage_RuntimeException() {

		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(centerMessageEntity);

		CenterMessageDto centerMessageDto = new CenterMessageDto();
		centerMessageDto.setCenterMessageId(messageId);

		assertThrows(RuntimeException.class, () -> {

			centerMessageService.createMessage(centerMessageDto);
		});
	}
	
	@Test
	final void testDeleteCenterMessage() throws Exception {
		
		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(centerMessageEntity);
		
		centerMessageService.deleteCenterMessage(messageId);
	
		verify(centerMessageRepository, times(1)).delete(any(CenterMessageEntity.class));
	}
	
	@Test
	final void testDeleteCenterMessage_Exeption() {
		
		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(null);
		
		assertThrows(Exception.class, () -> {
			
			centerMessageService.deleteCenterMessage(messageId);
		});
	}
	
	@Test
	final void testUpdateCenterMessage() throws NameNotFoundException {
		
		when(centerMessageRepository.findByCenterMessageId(anyString())).thenReturn(centerMessageEntity);
		
		when(centerMessageRepository.save(any(CenterMessageEntity.class))).thenReturn(centerMessageEntity);
		
		CenterMessageDto centerMessageDto = new CenterMessageDto();
		centerMessageDto.setText("Witaj!");
		
		CenterMessageDto updatedCenterMessageDto = centerMessageService.updateCenterMessage(messageId, centerMessageDto);
		assertEquals(centerMessageDto.getText(), updatedCenterMessageDto.getText());
		assertNotNull(updatedCenterMessageDto.getCenterMessageId());
		verify(centerMessageRepository, times(1)).save(any(CenterMessageEntity.class));
	}
}