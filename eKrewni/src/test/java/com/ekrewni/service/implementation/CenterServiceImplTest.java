package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.io.entity.AddressEntity;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.io.repository.BloodResourceRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.AddressDto;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.shared.dto.CenterDto;

class CenterServiceImplTest {
	
	@InjectMocks
	CenterServiceImpl centerService;
	
	@Mock
	CenterRepository centerRepository;

    @Mock
    BloodResourceRepository bloodResourceRepository;
    
    @Mock
    BloodRepository bloodRepository;
    
	@Mock
	RandomStringGenerator randomStringGenerator;
	
	String centerId = "jciaugfabvaivgfisf";
	String addressId = "kjxhSIvhif78ejkiu9";
	String bloodResourceId = "sdfetjg8988rt6j";
	
	CenterEntity centerEntity;
	AddressEntity addressEntity;
	Iterable<BloodEntity> bloodEntities;
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		centerEntity = new CenterEntity();
		centerEntity.setId(1L);
		centerEntity.setName("RCKiK");
		centerEntity.setCenterId(centerId);
		centerEntity.setEmail("rckik@gov.pl");
		centerEntity.setPhoneNumber("5326320");
		centerEntity.setWebSite("www.rckik.gov.pl");
		centerEntity.setAddress(getAddressEntity());
		centerEntity.setBloodResources(getBloodResourcesEntity());
	}

	@Test
	final void testGetCenterByCenterId() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		
		CenterDto centerDto = centerService.getCenterByCenterId(centerId);
		
		assertNotNull(centerDto);
		assertEquals("RCKiK", centerDto.getName());
	}
	
	@Test
	final void testGetCenterByCenterId_UsernameNotFoundException() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(null);
		
		assertThrows(UsernameNotFoundException.class, () -> {
			
			centerService.getCenterByCenterId(centerId);
		});
	}
	
	@Test
	final void testCreateCenter() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(null);
		when(randomStringGenerator.generateAddressId(anyInt())).thenReturn(addressId);
		when(randomStringGenerator.generateCenterId(anyInt())).thenReturn(centerId);
		when(randomStringGenerator.generateBloodResourceId(anyInt())).thenReturn(bloodResourceId);
		
		when(centerRepository.save(any(CenterEntity.class))).thenReturn(centerEntity);
		
		bloodEntities = bloodRepository.findAll();	
			
		CenterDto centerDto = new CenterDto();
		centerDto.setName("RCKiK");
		centerDto.setEmail("rckik@gov.pl");
		centerDto.setPhoneNumber("5326320");
		centerDto.setWebSite("www.rckik.gov.pl");
		centerDto.setBloodResources(getBloodResourcesDto());
		centerDto.setAddress(getAddressDto());
		
		CenterDto storedCenterDetails = centerService.createCenter(centerDto);
		assertNotNull(storedCenterDetails);
		assertEquals(centerEntity.getName(), storedCenterDetails.getName());
		assertEquals(centerEntity.getEmail(), storedCenterDetails.getEmail());
		assertEquals(centerEntity.getPhoneNumber(), storedCenterDetails.getPhoneNumber());
		assertEquals(centerEntity.getWebSite(), storedCenterDetails.getWebSite());
		assertNotNull(storedCenterDetails.getCenterId());
		assertEquals(storedCenterDetails.getBloodResources().size(), centerEntity.getBloodResources().size());
		verify(randomStringGenerator, times(1)).generateAddressId(30);
		verify(centerRepository, times(1)).save(any(CenterEntity.class));
	}
	
	@Test
	final void testCreateCenter_RuntimeException() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		
		CenterDto centerDto = new CenterDto();
		centerDto.setCenterId(centerId);
		
		assertThrows(RuntimeException.class, () -> {
					
			centerService.createCenter(centerDto);
		});
	}
	
	@Test
	final void testDeleteCenter() throws Exception {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		
		centerService.deleteCenter(centerId);
	
		verify(centerRepository, times(1)).delete(any(CenterEntity.class));
	}
	
	@Test
	final void testDeleteCenter_Exeption() {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(null);
		
		assertThrows(Exception.class, () -> {
			
			centerService.deleteCenter(centerId);
		});
	}
	
	@Test
	final void testUpdateCenter() throws NameNotFoundException {
		
		when(centerRepository.findByCenterId(anyString())).thenReturn(centerEntity);
		
		when(centerRepository.save(any(CenterEntity.class))).thenReturn(centerEntity);
		
		CenterDto centerDto = new CenterDto();
		centerDto.setName("RCKiK2");
		centerDto.setEmail("rckik2@gov.pl");
		centerDto.setPhoneNumber("5313569");
		centerDto.setWebSite("www.rckik2.gov.pl");
		
		CenterDto updatedCenterDto = centerService.updateCenter(centerId, centerDto);
		assertEquals(centerDto.getName(), updatedCenterDto.getName());
		assertEquals(centerDto.getEmail(), updatedCenterDto.getEmail());
		assertEquals(centerDto.getPhoneNumber(), updatedCenterDto.getPhoneNumber());
		assertEquals(centerDto.getWebSite(), updatedCenterDto.getWebSite());
		assertNotNull(updatedCenterDto.getCenterId());
		verify(centerRepository, times(1)).save(any(CenterEntity.class));
	}
	
	private AddressDto getAddressDto() {
		
		AddressDto addressDto = new AddressDto();
		addressDto.setCity("Tczew");
		addressDto.setStreet("Kasztanowa 15a");
		addressDto.setPostalCode("83-110");
		addressDto.setLatitude("123456789");
		addressDto.setLongitude("987654321");
		
		return addressDto;
	}
	
	private AddressEntity getAddressEntity() {
		
		AddressDto addressDto = getAddressDto();
		
		return new ModelMapper().map(addressDto, AddressEntity.class);
	}
	
	private List<BloodResourceDto> getBloodResourcesDto() {
		
		BloodResourceDto bloodResourceDto1 = new BloodResourceDto();
		bloodResourceDto1.setVolume(10);
		
		BloodResourceDto bloodResourceDto2 = new BloodResourceDto();
		bloodResourceDto2.setVolume(20);
		
		BloodResourceDto bloodResourceDto3 = new BloodResourceDto();
		bloodResourceDto3.setVolume(30);
		
		List<BloodResourceDto> bloodResources = new ArrayList<>();
		bloodResources.add(bloodResourceDto1);
		bloodResources.add(bloodResourceDto2);
		bloodResources.add(bloodResourceDto3);

		return bloodResources;
	}
	
	private List<BloodResourceEntity> getBloodResourcesEntity() {
		
		List<BloodResourceDto> bloodResources = getBloodResourcesDto();
		
		Type listType = new TypeToken<List<BloodResourceEntity>>() {}.getType();
		
		return new ModelMapper().map(bloodResources, listType);
	}
}