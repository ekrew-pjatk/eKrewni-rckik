package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.naming.NameNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.BloodDto;

class BloodServiceImplTest {

	@InjectMocks
	BloodServiceImpl bloodService;

	@Mock
	BloodRepository bloodRepository;

	@Mock
	RandomStringGenerator randomStringGenerator;

	String bloodId = "iu475y5thhgo284g";

	BloodEntity bloodEntity;

	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		bloodEntity = new BloodEntity();
		bloodEntity.setId(1L);
		bloodEntity.setBloodId(bloodId);
		bloodEntity.setBloodType("AB");
		bloodEntity.setRh("-");
	}

	@Test
	final void testGetBloodByBloodId() {

		when(bloodRepository.findByBloodId(anyString())).thenReturn(bloodEntity);

		BloodDto bloodDto = bloodService.getBloodByBloodId(bloodId);

		assertNotNull(bloodDto);
		assertEquals("AB", bloodDto.getBloodType());
	}

	@Test
	final void testGetBloodByBloodId_UsernameNotFoundException() {

		when(bloodRepository.findByBloodId(anyString())).thenReturn(null);

		assertThrows(UsernameNotFoundException.class, () -> {

			bloodService.getBloodByBloodId(bloodId);
		});
	}

	@Test
	final void testCreateBloodGroup() {

		when(bloodRepository.findByBloodTypeAndRh(anyString(), anyString())).thenReturn(null);
		when(randomStringGenerator.generateBloodId(anyInt())).thenReturn(bloodId);

		when(bloodRepository.save(any(BloodEntity.class))).thenReturn(bloodEntity);

		BloodDto bloodDto = new BloodDto();
		bloodDto.setBloodType("AB");
		bloodDto.setRh("-");

		BloodDto storedBloodDetails = bloodService.createBloodGroup(bloodDto);
		assertNotNull(storedBloodDetails);
		assertEquals(bloodEntity.getBloodType(), storedBloodDetails.getBloodType());
		assertEquals(bloodEntity.getRh(), storedBloodDetails.getRh());
		assertNotNull(storedBloodDetails.getBloodId());
		verify(randomStringGenerator, times(1)).generateBloodId(30);
		verify(bloodRepository, times(1)).save(any(BloodEntity.class));
	}

	@Test
	final void testCreateBloodGroup_CenterUserServiceException() {

		when(bloodRepository.findByBloodTypeAndRh(anyString(), anyString())).thenReturn(bloodEntity);

		BloodDto bloodDto = new BloodDto();
		bloodDto.setBloodType("AB");
		bloodDto.setRh("-");

		assertThrows(CenterUserServiceException.class, () -> {

			bloodService.createBloodGroup(bloodDto);
		});
	}
	
	@Test
	final void testDeleteBlood() throws Exception {
		
		when(bloodRepository.findByBloodId(anyString())).thenReturn(bloodEntity);
		
		bloodService.deleteBlood(bloodId);
	
		verify(bloodRepository, times(1)).delete(any(BloodEntity.class));
	}
	
	@Test
	final void testDeleteBlood_Exeption() {
		
		when(bloodRepository.findByBloodId(anyString())).thenReturn(null);
		
		assertThrows(Exception.class, () -> {
			
			bloodService.deleteBlood(bloodId);
		});
	}
	
	@Test
	final void testUpdateBlood() throws NameNotFoundException {
		
		when(bloodRepository.findByBloodId(anyString())).thenReturn(bloodEntity);
		
		when(bloodRepository.save(any(BloodEntity.class))).thenReturn(bloodEntity);
		
		BloodDto bloodDto = new BloodDto();
		bloodDto.setBloodType("B");
		bloodDto.setRh("+");
		
		BloodDto updatedBloodDto = bloodService.updateBlood(bloodId, bloodDto);
		assertEquals(bloodDto.getBloodType(), updatedBloodDto.getBloodType());
		assertEquals(bloodDto.getRh(), updatedBloodDto.getRh());
		assertNotNull(updatedBloodDto.getBloodId());
		verify(bloodRepository, times(1)).save(any(BloodEntity.class));
	}
}