package com.ekrewni.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.entity.RoleEntity;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.io.repository.RoleRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.CenterUserDto;

class CenterUserServiceImplTest {

	@InjectMocks
	CenterUserServiceImpl centerUserService;

	@Mock
	CenterUserRepository centerUserRepository;
	
	@Mock
	CenterRepository centerRepository;

	@Mock
	RoleRepository roleRepository;
	
	@Mock
	RandomStringGenerator randomStringGenerator;

	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;

	String centerUserId = "2zn6m2rYPCMstuXfHW4ZAYXK64Fpra";
	String encryptedPassword = "$2a$10$XRFTVyvUPM1i0k88plTMc./1/c2NBPzDxiha5i.kZA4luPRD/VNtu";

	CenterUserEntity centerUserEntity;
	RoleEntity roleEntity;
	
	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		centerUserEntity = new CenterUserEntity();
		centerUserEntity.setId(1L);
		centerUserEntity.setUserId(centerUserId);
		centerUserEntity.setLogin("TechAdmin");
		centerUserEntity.setEncryptedPassword(encryptedPassword); 
		centerUserEntity.setRoles(getRolesEntity());
	}

	@Test
	final void testGetCenterUserByUserId() {

		when(centerUserRepository.findByUserId(anyString())).thenReturn(centerUserEntity);
		
		CenterUserDto centerUserDto = centerUserService.getCenterUserByUserId(centerUserId);
		
		assertNotNull(centerUserDto);
		assertEquals("TechAdmin", centerUserDto.getLogin());
	}
	
	@Test
	final void testGetCenterUserByUserId_UsernameNotFoundException() {
		
		when(centerUserRepository.findByUserId(anyString())).thenReturn(null);

		assertThrows(UsernameNotFoundException.class, () -> {
			
			centerUserService.getCenterUserByUserId(centerUserId);
		});
	}
	
	@Test
	final void testCreateCenterUser() {
		
		when(centerUserRepository.findByLogin(anyString())).thenReturn(null);
		when(randomStringGenerator.generateCenterUserId(anyInt())).thenReturn(centerUserId);
		when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
		when(roleRepository.findByName(anyString())).thenReturn(roleEntity);
		
		when(centerUserRepository.save(any(CenterUserEntity.class))).thenReturn(centerUserEntity);
		
		CenterUserDto centerUserDto = new CenterUserDto();
		centerUserDto.setLogin("TechAdmin");
		centerUserDto.setPassword("123456789");
		centerUserDto.setRoles(getRoles());
		
		CenterUserDto storedCenterUserDetails = centerUserService.createCenterUser(centerUserDto);
		assertNotNull(storedCenterUserDetails);
		assertEquals(centerUserEntity.getLogin(), storedCenterUserDetails.getLogin());
		assertNotNull(storedCenterUserDetails.getUserId());
		assertEquals(storedCenterUserDetails.getRoles().size(), centerUserEntity.getRoles().size());
		verify(randomStringGenerator, times(1)).generateCenterUserId(30);
		verify(bCryptPasswordEncoder, times(1)).encode("123456789");
		verify(centerUserRepository, times(1)).save(any(CenterUserEntity.class));
	}
	
	@Test
	final void testCreateCenterUser_CenterUserServiceException() {
		
		when(centerUserRepository.findByLogin(anyString())).thenReturn(centerUserEntity);
		
		CenterUserDto centerUserDto = new CenterUserDto();
		centerUserDto.setLogin("TechAdmin");
		
		assertThrows(CenterUserServiceException.class, () -> {
					
			centerUserService.createCenterUser(centerUserDto);
		});
	}
	
	@Test
	final void testDeleteCenterUser() throws Exception {
		
		when(centerUserRepository.findByUserId(anyString())).thenReturn(centerUserEntity);
		
		centerUserService.deleteCenterUser(centerUserId);
	
		verify(centerUserRepository, times(1)).delete(any(CenterUserEntity.class));
	}
	
	@Test
	final void testDeleteCenterUser_CenterUserServiceExeption() {
		
		when(centerUserRepository.findByUserId(anyString())).thenReturn(null);
		
		assertThrows(CenterUserServiceException.class, () -> {
			
			centerUserService.deleteCenterUser(centerUserId);
		});
	}
	
	@Test
	final void testUpdateCenterUser() {
		
		when(centerUserRepository.findByUserId(anyString())).thenReturn(centerUserEntity);
		when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
		
		when(centerUserRepository.save(any(CenterUserEntity.class))).thenReturn(centerUserEntity);
		
		CenterUserDto centerUserDto = new CenterUserDto();
		centerUserDto.setPassword("123456789");
		centerUserDto.setEncryptedPassword(encryptedPassword);
		
		CenterUserDto updatedCenterUserDto = centerUserService.updateCenterUser(centerUserId, centerUserDto);
		assertEquals(centerUserDto.getEncryptedPassword(), updatedCenterUserDto.getEncryptedPassword());
		assertNotNull(updatedCenterUserDto.getUserId());
		verify(bCryptPasswordEncoder, times(1)).encode("123456789");
		verify(centerUserRepository, times(1)).save(any(CenterUserEntity.class));
	}
	
	private Collection<String> getRoles() {
		
		Collection<String> roles = new ArrayList<>();
		roles.add("ROLE_TECH_ADMIN");
		roles.add("ROLE_MED_ADMIN");
		
		return roles;
	}
	
	private Collection<RoleEntity> getRolesEntity() {
		
		Collection<String> roles = getRoles();
		
		Type listType = new TypeToken<List<RoleEntity>>() {}.getType();
		
		return new ModelMapper().map(roles, listType);
	}
}