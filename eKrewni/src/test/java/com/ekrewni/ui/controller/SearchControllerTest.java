package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.AlertService;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.ui.model.response.SearchAlertRest;

class SearchControllerTest {

	@InjectMocks
	SearchController searchController;
	
	@Mock
	AlertService alertService;
	
	AlertDto alertDto;
	
	final String ALERT_ID = "tbgw4tkhj09";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		alertDto = new AlertDto();
		alertDto.setAlertId(ALERT_ID);
		alertDto.setMessage("Brakuje krwi!");
	}

	@Test
	void testGetAlertWithCenterId() {
		
		when(alertService.getAlertByAlertId(anyString())).thenReturn(alertDto);
		
		SearchAlertRest alertRest = searchController.getAlertWithCenterId(ALERT_ID);
		assertNotNull(alertRest);
		assertEquals(ALERT_ID, alertRest.getAlertId());
		assertEquals(alertDto.getMessage(), alertRest.getMessage());
	}

}
