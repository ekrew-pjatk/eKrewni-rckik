package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.AlertService;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.ui.model.response.AlertRest;

class AlertControllerTest {
	
	@InjectMocks
	AlertController alertController;

	@Mock
	AlertService alertService;
	
	AlertDto alertDto;
	
    final String ALERT_ID = "jfhvw4o9kjt8u294";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		alertDto = new AlertDto();
		
		alertDto.setAlertId(ALERT_ID);
		alertDto.setMessage("Mało krwi!!!");
	}

	@Test
	final void testGetAlert() {
		
		when(alertService.getAlertByAlertId(anyString())).thenReturn(alertDto);
		
		AlertRest alertRest = alertController.getAlert(ALERT_ID);
		assertNotNull(alertRest);
		assertEquals(ALERT_ID, alertRest.getAlertId());
		assertEquals(alertDto.getMessage(), alertRest.getMessage());
	}
}
