package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.CenterService;
import com.ekrewni.shared.dto.CenterDto;
import com.ekrewni.ui.model.response.CenterRest;

class CenterControllerTest {
	
	@InjectMocks
	CenterController centerController;
	
	@Mock
	CenterService centerService;
	
	CenterDto centerDto;

	final String CENTER_ID = "juierhvb387";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		centerDto = new CenterDto();
		centerDto.setCenterId(CENTER_ID);
		centerDto.setName("RCKiK");
		centerDto.setPhoneNumber("123456789");
		centerDto.setWebSite("www.rckik.gov.pl");
		centerDto.setEmail("rckik@gov.pl");
	}

	@Test
	final void testGetCenter() {
		
		when(centerService.getCenterByCenterId(anyString())).thenReturn(centerDto);
		
		CenterRest centerRest = centerController.getCenter(CENTER_ID);
		assertNotNull(centerRest);
		assertEquals(CENTER_ID, centerRest.getCenterId());
		assertEquals(centerDto.getName(), centerRest.getName());
		assertEquals(centerDto.getPhoneNumber(), centerRest.getPhoneNumber());
		assertEquals(centerDto.getWebSite(), centerRest.getWebSite());
		assertEquals(centerDto.getEmail(), centerRest.getEmail());
	}

}
