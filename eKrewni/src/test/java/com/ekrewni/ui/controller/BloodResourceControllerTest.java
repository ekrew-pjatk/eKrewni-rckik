package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.BloodResourceService;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.ui.model.response.BloodResourceRest;

class BloodResourceControllerTest {

	@InjectMocks
	BloodResourceController bloodResourceController;
	
	@Mock
	BloodResourceService bloodResourceService;
	
	BloodResourceDto bloodResourceDto;
	
	final String BLOODRESOURCE_ID = "eihgq375gh9h138gh";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		bloodResourceDto = new BloodResourceDto();
		bloodResourceDto.setVolume(250);
		bloodResourceDto.setBloodResourceId(BLOODRESOURCE_ID);
	}

	@Test
	final void testGetBloodResource() {
		
		when(bloodResourceService.getBloodResourceByBloodResourceId(anyString())).thenReturn(bloodResourceDto);
		
		BloodResourceRest bloodResourceRest = bloodResourceController.getBloodResource(BLOODRESOURCE_ID);
		assertNotNull(bloodResourceRest);
		assertEquals(BLOODRESOURCE_ID, bloodResourceRest.getBloodResourceId());
		assertEquals(bloodResourceDto.getVolume(), bloodResourceRest.getVolume());
	}
}
