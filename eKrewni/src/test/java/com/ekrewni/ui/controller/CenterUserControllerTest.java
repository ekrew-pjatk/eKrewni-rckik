package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.CenterUserService;
import com.ekrewni.shared.dto.CenterUserDto;
import com.ekrewni.ui.model.response.CenterUserRest;

class CenterUserControllerTest {

	@InjectMocks
	CenterUserController centerUserController;
	
	@Mock
	CenterUserService userService;
	
	CenterUserDto centerUserDto;
	
	final String USER_ID = "jdfhg7485h42gh2";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		centerUserDto = new CenterUserDto();
		centerUserDto.setLogin("TechAdmin");
		centerUserDto.setUserId(USER_ID);
		centerUserDto.setEncryptedPassword("jv5v7984");
	}

	@Test
	final void testGetCenterUser() {
		
		when(userService.getCenterUserByUserId(anyString())).thenReturn(centerUserDto);
		
		CenterUserRest centerUserRest = centerUserController.getCenterUser(USER_ID);
		assertNotNull(centerUserRest);
		assertEquals(USER_ID, centerUserRest.getUserId());
		assertEquals(centerUserDto.getLogin(), centerUserRest.getLogin());
	}
}
