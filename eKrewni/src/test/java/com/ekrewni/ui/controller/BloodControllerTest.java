package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.BloodService;
import com.ekrewni.shared.dto.BloodDto;
import com.ekrewni.ui.model.response.BloodRest;

class BloodControllerTest {
	
	@InjectMocks
	BloodController bloodController;
	
	@Mock
	BloodService bloodService;
	
	BloodDto bloodDto;

	final String BLOOD_ID = "fgwjr53875";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		bloodDto = new BloodDto();
		
		bloodDto.setBloodId(BLOOD_ID);
		bloodDto.setBloodType("A");
		bloodDto.setRh("+");
	}

	@Test
	final void testGetBloodGroup() {
		
		when(bloodService.getBloodByBloodId(anyString())).thenReturn(bloodDto);
		
		BloodRest bloodRest = bloodController.getBloodGroup(BLOOD_ID);
		assertNotNull(bloodRest);
		assertEquals(BLOOD_ID, bloodRest.getBloodId());
		assertEquals(bloodDto.getRh(), bloodRest.getRh());
	}
}
