package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.AddressService;
import com.ekrewni.shared.dto.AddressDto;
import com.ekrewni.ui.model.response.CoordinatesRest;

class CoordinatesControllerTest {

	@InjectMocks
	CoordinatesController coordinatesController;
	
	@Mock
	AddressService addressService;
	
	AddressDto addressDto1;
	AddressDto addressDto2;
	AddressDto addressDto3;
	List<AddressDto> addressesDto;
	
	final String CITY1 = "Gdańsk";
	final String CITY2 = "Wrocław";
	final String CITY3 = "Kalisz";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		addressDto1 = new AddressDto();
		addressDto1.setCity(CITY1);
		addressDto1.setLatitude("123456789");
		addressDto1.setLongitude("987654321");
		
		addressDto2 = new AddressDto();
		addressDto2.setCity(CITY2);
		addressDto2.setLatitude("135790864");
		addressDto2.setLongitude("975312468");
		
		addressDto3 = new AddressDto();
		addressDto3.setCity(CITY3);
		addressDto3.setLatitude("147096325");
		addressDto3.setLongitude("963258104");
		
		addressesDto = new ArrayList<>();
		addressesDto.add(addressDto1);
		addressesDto.add(addressDto2);
		addressesDto.add(addressDto3);
	}

	@Test
	final void testGetAllLongLatCoordinates() {
		
		when(addressService.getAddresses(anyInt(), anyInt())).thenReturn(addressesDto);
		
		List<CoordinatesRest> coordinatesRest = coordinatesController.getAllLongLatCoordinates(0,  25);
		assertNotNull(coordinatesRest);
		assertEquals(CITY1, coordinatesRest.get(0).getCity());
		assertEquals(addressesDto.get(1).getLatitude(), coordinatesRest.get(1).getLatitude());
		assertEquals(addressesDto.get(2).getLongitude(), coordinatesRest.get(2).getLongitude());
		assertEquals(coordinatesRest.size(), addressesDto.size());
	}
}
