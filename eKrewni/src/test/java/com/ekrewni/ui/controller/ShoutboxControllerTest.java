package com.ekrewni.ui.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ekrewni.service.CenterMessageService;
import com.ekrewni.shared.dto.CenterMessageDto;
import com.ekrewni.ui.model.response.CenterMessageRest;

class ShoutboxControllerTest {

	@InjectMocks
	ShoutboxController shoutboxController;
	
	@Mock
	CenterMessageService centerMessageService;
	
	CenterMessageDto centerMessageDto1;
	CenterMessageDto centerMessageDto2;
	CenterMessageDto centerMessageDto3;
	List<CenterMessageDto> centerMessagesDto;
	
	final String MESSAGE1_ID = "sjkfhwaugw87ghy";
	final String MESSAGE2_ID = "kdfjgkqghu4o35g";
	final String MESSAGE3_ID = "djkehu31hg8g4gc";
	
	@BeforeEach
	void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		centerMessageDto1 = new CenterMessageDto();
		centerMessageDto1.setCenterMessageId(MESSAGE1_ID);
		centerMessageDto1.setText("Hello!");
		
		centerMessageDto2 = new CenterMessageDto();
		centerMessageDto2.setCenterMessageId(MESSAGE2_ID);
		centerMessageDto2.setText("Hi!");
		
		centerMessageDto3 = new CenterMessageDto();
		centerMessageDto3.setCenterMessageId(MESSAGE2_ID);
		centerMessageDto3.setText("Bye!");
		
		centerMessagesDto = new ArrayList<>();
		centerMessagesDto.add(centerMessageDto1);
		centerMessagesDto.add(centerMessageDto2);
		centerMessagesDto.add(centerMessageDto3);
	}

	@Test
	final void testGetCentersMessages() {
		
		when(centerMessageService.getCentersMessages(anyInt(), anyInt())).thenReturn(centerMessagesDto);
		
		List<CenterMessageRest> centerMessagesRest = shoutboxController.getCentersMessages(0, 25);
		assertNotNull(centerMessagesRest);
		assertEquals(MESSAGE1_ID, centerMessagesRest.get(0).getCenterMessageId());
		assertEquals(centerMessagesDto.get(1).getText(), centerMessagesRest.get(1).getText());
		assertEquals(centerMessagesRest.size(), centerMessagesDto.size());
	}
}
