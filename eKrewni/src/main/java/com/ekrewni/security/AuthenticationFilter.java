package com.ekrewni.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ekrewni.ui.model.request.CenterUserLoginRequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;

	public AuthenticationFilter(AuthenticationManager authenticationManager) {

		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		try {

			CenterUserLoginRequestModel centerUserLoginRequestModel = new ObjectMapper()
					.readValue(request.getInputStream(), CenterUserLoginRequestModel.class);

			return authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(centerUserLoginRequestModel.getLogin(),
							centerUserLoginRequestModel.getPassword(), new ArrayList<>()));

		} catch (IOException exception) {

			throw new RuntimeException(exception);
		}

	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			FilterChain filterChain, Authentication authentication) throws IOException, ServletException {

		String userName = ((UserPrincipal) authentication.getPrincipal()).getUsername();
		String userId = ((UserPrincipal) authentication.getPrincipal()).getCenterUserId();	

		String token = Jwts.builder().setSubject(userName)
				.setExpiration(new Date(System.currentTimeMillis() + StaticFields.TIME))
				.signWith(SignatureAlgorithm.HS512, StaticFields.getTokenSecret()).compact();

		response.addHeader(StaticFields.HEADER, StaticFields.PREFIX_OF_TOKEN + token);
		response.addHeader("CenterUserID", userId);
	}
	
}
