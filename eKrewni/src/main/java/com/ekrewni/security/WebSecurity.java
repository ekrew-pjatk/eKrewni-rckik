package com.ekrewni.security;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
//
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.service.CenterUserService;

@EnableGlobalMethodSecurity(securedEnabled = true)
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private final CenterUserService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final CenterUserRepository centerUserRepository;

	public WebSecurity(CenterUserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder,
			CenterUserRepository centerUserRepository) {

		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.centerUserRepository = centerUserRepository;

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		
		http.cors().and().csrf().disable().authorizeRequests()

				// SWAGGER
				.antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**").permitAll()

				// ALERTS
				.antMatchers(HttpMethod.GET, "/alerts", "/alerts/*", "/search", "/search/*").permitAll()
				.antMatchers(HttpMethod.POST, "/alerts").hasAnyRole("USER", "MED_ADMIN")
				.antMatchers(HttpMethod.PUT, "/alerts/*").hasAnyRole("USER", "MED_ADMIN")
				.antMatchers(HttpMethod.DELETE, "/alerts/*").hasAnyRole("USER", "MED_ADMIN")

				// AUTH
				.antMatchers(HttpMethod.POST, StaticFields.LOGIN_URL).permitAll()

				// BLOOD GROUPS
				.antMatchers(HttpMethod.GET, "/bloodgroups", "/bloodgroups/*").permitAll()
				.antMatchers(HttpMethod.POST, "/bloodgroups").hasRole("MED_ADMIN")
				.antMatchers(HttpMethod.PUT, "/bloodgroups/*").hasRole("MED_ADMIN")
				.antMatchers(HttpMethod.DELETE, "/bloodgroups/*").hasRole("MED_ADMIN")

				// BLOOD RESOURCES
				.antMatchers(HttpMethod.GET, "/bloodresources/**").permitAll()
				.antMatchers(HttpMethod.POST, "/bloodresources").hasAnyRole("MED_ADMIN")
				.antMatchers(HttpMethod.PUT, "/bloodresources/**").hasAnyRole("USER", "MED_ADMIN")
				.antMatchers(HttpMethod.DELETE, "/bloodresources/*").hasRole("MED_ADMIN")

				// CENTERS
				.antMatchers(HttpMethod.GET, "/centers", "/centers/**").permitAll()
				.antMatchers(HttpMethod.POST, "/centers/**").hasRole("TECH_ADMIN")
				.antMatchers(HttpMethod.PUT, "/centers/**").hasRole("TECH_ADMIN")
				.antMatchers(HttpMethod.DELETE, "/centers/**").hasRole("TECH_ADMIN")

				// CENTER USERS
				.antMatchers(HttpMethod.GET, "/centerusers/*/center").permitAll()
				.antMatchers(HttpMethod.GET, "/centerusers", "/centerusers/**").hasRole("TECH_ADMIN")
				.antMatchers(HttpMethod.POST, "/centerusers/**").hasRole("TECH_ADMIN")
				.antMatchers(HttpMethod.PUT, "/centerusers/**").hasAnyRole("USER", "TECH_ADMIN")
				.antMatchers(HttpMethod.DELETE, "/centerusers/*").hasRole("TECH_ADMIN")
				

				// COORDINATES
				.antMatchers(HttpMethod.GET, "/coordinates","/coordinates/**").permitAll()
				
				// INFO
				.antMatchers(HttpMethod.GET, "/info","/info/**").permitAll()

				// SHOUTBOX
				.antMatchers(HttpMethod.GET, "/shoutbox/**").hasAnyRole("USER", "TECH_ADMIN")
				.antMatchers(HttpMethod.POST, "/shoutbox/**").hasAnyRole("USER", "TECH_ADMIN")

				.anyRequest().authenticated().and().addFilter(getAuthenticationFilter())
				.addFilter(new AuthorizationFilter(authenticationManager(), centerUserRepository)).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS); // MAKE REST API STATELESS -> we don't want http session to be created
		http.headers().frameOptions().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

		authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);

	}

	public AuthenticationFilter getAuthenticationFilter() throws Exception {

		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());
		filter.setFilterProcessesUrl("/centerusers/login");
		return filter;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {

		final CorsConfiguration corsConfiguration = new CorsConfiguration();

		corsConfiguration.setAllowedOrigins(Arrays.asList("*"));
		corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedHeaders(Arrays.asList("*"));
		corsConfiguration.setExposedHeaders(Arrays.asList("authorization", "centerUserId"));

		final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

		return urlBasedCorsConfigurationSource;
	}

	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {

		return super.authenticationManagerBean();
	}

}
