package com.ekrewni.security;

import com.ekrewni.SpringApplicationContext;

public class StaticFields {

	public static final long TIME = 864000000; // 10 days
	public static final String PREFIX_OF_TOKEN = "Holder ";
	public static final String HEADER = "Authorization";
	public static final String ALERT_URL = "/alerts";
	public static final String CENTER_USER_SIGN_UP_URL = "/centerusers";
	public static final String LOGIN_URL = "/centerusers/login";
	public static final String BLOODS = "/bloodgroups";

	public static String getTokenSecret() {

		AppProperties eKrewniApplicationProperties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
		return eKrewniApplicationProperties.getTokenSecret();
	}
}
