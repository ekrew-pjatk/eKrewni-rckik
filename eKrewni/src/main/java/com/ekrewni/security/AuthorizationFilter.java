package com.ekrewni.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.CenterUserRepository;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {

	private final CenterUserRepository centerUserRepository;

	public AuthorizationFilter(AuthenticationManager authenticationManager, CenterUserRepository centerUserRepository) {

		super(authenticationManager);
		this.centerUserRepository = centerUserRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		String header = request.getHeader(StaticFields.HEADER);

		if (header == null || !header.startsWith(StaticFields.PREFIX_OF_TOKEN)) {

			filterChain.doFilter(request, response);
			return;
		}

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getAuthentication(request);
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		filterChain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {

		String token = request.getHeader(StaticFields.HEADER);

		if (token != null) {

			token = token.replace(StaticFields.PREFIX_OF_TOKEN, "");

			String user = Jwts.parser().setSigningKey(StaticFields.getTokenSecret()).parseClaimsJws(token).getBody()
					.getSubject();

			if (user != null) {

				CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(user);

				UserPrincipal userPrincipal = new UserPrincipal(centerUserEntity);
				return new UsernamePasswordAuthenticationToken(user, null, userPrincipal.getAuthorities());

			}

			return null;
		}

		return null;

	}

}
