package com.ekrewni.security;

import java.util.Collection;
import java.util.HashSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ekrewni.io.entity.AuthorityEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.entity.RoleEntity;

public class UserPrincipal implements UserDetails {

	private static final long serialVersionUID = 9067791452626535327L;

	CenterUserEntity centerUserEntity;

	private String centerUserId;

	public UserPrincipal(CenterUserEntity centerUserEntity) {

		this.centerUserEntity = centerUserEntity;

		this.setCenterUserId(centerUserEntity.getUserId());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		Collection<GrantedAuthority> authorities = new HashSet<>();
		Collection<AuthorityEntity> authorityEntities = new HashSet<>();

		// Get user Roles
		Collection<RoleEntity> roles = centerUserEntity.getRoles();

		if (roles == null) {

			return authorities;
		}

		roles.forEach((role) -> {

			authorities.add(new SimpleGrantedAuthority(role.getName()));
			authorityEntities.addAll(role.getAuthorities());
		});

		authorityEntities.forEach((authorityEntity) -> {

			authorities.add(new SimpleGrantedAuthority(authorityEntity.getName()));
		});

		return authorities;
	}

	@Override
	public String getPassword() {

		return this.centerUserEntity.getEncryptedPassword();
	}

	@Override
	public String getUsername() {

		return this.centerUserEntity.getLogin();
	}

	@Override
	public boolean isAccountNonExpired() {

		return true;
	}

	@Override
	public boolean isAccountNonLocked() {

		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return true;
	}

	@Override
	public boolean isEnabled() {

		return true;
	}

	public String getCenterUserId() {
		return centerUserId;
	}

	public void setCenterUserId(String centerUserId) {
		this.centerUserId = centerUserId;
	}

}
