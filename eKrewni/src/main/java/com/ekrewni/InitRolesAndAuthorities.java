package com.ekrewni;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ekrewni.io.entity.AuthorityEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.entity.RoleEntity;
import com.ekrewni.io.repository.AuthorityRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.io.repository.RoleRepository;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.UsersRoles;

@Component
public class InitRolesAndAuthorities {

	@Autowired
	InitData initData;

	@Autowired
	AuthorityRepository authorityRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	CenterUserRepository centerUserRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@EventListener
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event) {

		AuthorityEntity readAuthority = createAuthority("READ_PRIVILEGE");
		AuthorityEntity writeAuthority = createAuthority("WRITE_PRIVILEGE");
		AuthorityEntity deleteAuthority = createAuthority("DELETE_PRIVILEGE");

		RoleEntity roleTechAdmin = createRole(UsersRoles.ROLE_TECH_ADMIN.name(),
				Arrays.asList(readAuthority, writeAuthority, deleteAuthority));
		RoleEntity roleMedAdmin = createRole(UsersRoles.ROLE_MED_ADMIN.name(),
				Arrays.asList(readAuthority, writeAuthority, deleteAuthority));
		RoleEntity roleUser = createRole(UsersRoles.ROLE_USER.name(), Arrays.asList(readAuthority, writeAuthority));

		if (roleTechAdmin == null) {

			return;
		}

		if (centerUserRepository.findByLogin("techAdmin") == null
				|| centerUserRepository.findByLogin("medAdmin") == null) {

			CenterUserEntity techAdminUser = new CenterUserEntity();
			techAdminUser.setLogin("techAdmin");
			techAdminUser.setUserId(randomStringGenerator.generateCenterUserId(30));
			techAdminUser.setEncryptedPassword(bCryptPasswordEncoder.encode("techAdmin"));
			techAdminUser.setRoles(Arrays.asList(roleTechAdmin));
			centerUserRepository.save(techAdminUser);

			CenterUserEntity medAdminUser = new CenterUserEntity();
			medAdminUser.setLogin("medAdmin");
			medAdminUser.setUserId(randomStringGenerator.generateCenterUserId(30));
			medAdminUser.setEncryptedPassword(bCryptPasswordEncoder.encode("medAdmin"));
			medAdminUser.setRoles(Arrays.asList(roleMedAdmin));
			centerUserRepository.save(medAdminUser);
		}

		CenterUserEntity ordinaryUser = new CenterUserEntity();
		ordinaryUser.setRoles(Arrays.asList(roleUser));

		initData.loadBloodgroupsAndVolumeOfResources();
		initData.loadCentersAndUsers(ordinaryUser);

	}

	@Transactional
	private AuthorityEntity createAuthority(String name) {

		AuthorityEntity authority = authorityRepository.findByName(name);

		if (authority == null) {

			authority = new AuthorityEntity(name);
			authorityRepository.save(authority);
		}
		return authority;
	}

	@Transactional
	private RoleEntity createRole(String name, Collection<AuthorityEntity> authorities) {

		RoleEntity role = roleRepository.findByName(name);

		if (role == null) {

			role = new RoleEntity(name);
			role.setAuthorities(authorities);
			roleRepository.save(role);
		}
		return role;
	}
}