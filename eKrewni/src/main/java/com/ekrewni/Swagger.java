package com.ekrewni;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger {

	List<VendorExtension> vendorExtension = new ArrayList<>();

	ApiInfo information = new ApiInfo("eKrewni application REST documentation",
			"On this pages are informations about eKrewni application endpoints", "1.0", null, null, "Apache 2.0",
			"http://www.apache.org/licenses/LICENSE-2.0", vendorExtension);

	@Bean
	public Docket eKrewniApplicationDocket() {

		Docket eKrewniDocket = new Docket(DocumentationType.SWAGGER_2).protocols(new HashSet<>(Arrays.asList("HTTP")))
				.apiInfo(information).select().apis(RequestHandlerSelectors.basePackage("com.ekrewni"))
				.paths(PathSelectors.any()).build();

		return eKrewniDocket;
	}

}
