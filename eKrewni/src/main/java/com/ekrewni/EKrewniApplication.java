package com.ekrewni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ekrewni.security.AppProperties;

@SpringBootApplication
public class EKrewniApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		return application.sources(EKrewniApplication.class);
	}

	public static void main(String[] args) {

		SpringApplication.run(EKrewniApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {

		return new BCryptPasswordEncoder();
	}

	@Bean
	public SpringApplicationContext springApplicationContext() {

		return new SpringApplicationContext();
	}

	@Bean(name = "AppProperties")
	public AppProperties getEKrewniApplicationProperties() {

		return new AppProperties();
	}

}
