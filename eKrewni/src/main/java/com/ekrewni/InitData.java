package com.ekrewni;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.ekrewni.io.entity.AddressEntity;
import com.ekrewni.io.entity.AlertEntity;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;
//import com.ekrewni.io.entity.CenterMessageEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.entity.RoleEntity;
import com.ekrewni.io.repository.AddressRepository;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.io.repository.BloodResourceRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.shared.RandomStringGenerator;

@Component
public class InitData {

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	BloodRepository bloodRepository;
	@Autowired
	BloodResourceRepository bloodResourceRepository;
	@Autowired
	CenterRepository centerRepository;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	CenterUserRepository centerUserRepository;
	@Autowired
	RandomStringGenerator randomStringGenerator;

	public void loadBloodgroupsAndVolumeOfResources() {

		String groupA = "A";
		String groupB = "B";
		String groupAB = "AB";
		String group0 = "0";
		String plus = "+";
		String minus = "-";

		if (bloodRepository.count() == 0) {

			saveToBloodRepository(group0, minus);
			saveToBloodRepository(group0, plus);
			saveToBloodRepository(groupA, minus);
			saveToBloodRepository(groupA, plus);
			saveToBloodRepository(groupB, minus);
			saveToBloodRepository(groupB, plus);
			saveToBloodRepository(groupAB, minus);
			saveToBloodRepository(groupAB, plus);
		}

	}

	public void loadCentersAndUsers(CenterUserEntity centerUserEntity) {

		long centerTuple = centerRepository.count();
		long addressTuple = addressRepository.count();
		long centerUserTuple = centerUserRepository.count();

		Collection<RoleEntity> roles = centerUserEntity.getRoles();

		if (centerTuple == 0 && addressTuple == 0 && centerUserTuple == 2) {

			saveToCenterAndAddressAndCenterUserRepositories("Białystok", "15-950", "M. Skłodowskiej-Curie 23",
					"53.125764", "23.162688", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Białymstoku",
					"(85)7447002", "sekretariat@rckik.bialystok.pl", "http://www.rckik.bialystok.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Bydgoszcz", "85-015", "Markwarta 8", "53.126154",
					"18.012266", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Bydgoszczy", "(52)3226517",
					"krew@rckik-bydgoszcz.com.pl", "http://www.rckik-bydgoszcz.com.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Gdańsk", "80-210", "Hoene Wrońskiego 4", "54.365743",
					"18.629042", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Gdańsku", "(58)5204040",
					"sekretariat@krew.gda.pl", "http://krew.gda.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Kalisz", "62-800", "Kaszubska 9", "51.770126", "18.103664",
					"Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Kaliszu", "(62)7679415",
					"sekretariat@krwiodawstwo.kalisz.pl", "http://krwiodawstwo.kalisz.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Katowice", "40-074", "Raciborska 15", "50.254879",
					"19.007029", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Katowicach", "(32)2087300",
					"rckik@rckik-katowice.pl", "http://www.rckik-katowice.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Kielce", "25-734", "Jagiellońska 66", "50.873227",
					"20.605319", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Kielcach", "(41)3359401",
					"sekretariat@rckik-kielce.com.pl", "http://www.rckik-kielce.com.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Kraków", "31-540", "Rzeźnicza 11", "50.056125",
					"19.956288", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Krakowie", "(12)2618820",
					"sekretariat@rckik.krakow.pl", "http://rckik.krakow.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Lublin", "20-078", "Żołnierzy Niepodległej 8", "51.248608",
					"22.556236", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Lublinie", "(81)5325318",
					"sekretariat@rckik.lublin.pl", "http://www.rckik.lublin.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Łódź", "91-433", "Franciszkańska 17/25", "51.782145",
					"19.461435", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Łodzi", "(42)6161400",
					"sekretariat@krwiodawstwo.pl", "http://www.krwiodawstwo.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Olsztyn", "10-247", "Malborska 2", "53.793462",
					"20.489986", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Olsztynie", "(89)5266302",
					"sekretariat@rckikol.pl", "http://www.rckikol.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Opole", "45-372", "Augustyna Kośnego 55", "50.670591",
					"17.939020", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Opolu", "(77)4413803",
					"sekretariat@rckik-opole.com.pl", "http://www.rckik-opole.com.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Poznań", "60-354", "Marcelińska 44", "52.404261",
					"16.883090", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Poznaniu", "(61)8863332",
					"sekretariat@rckik.poznan.pl", "http://www.rckik.poznan.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Racibórz", "47-400", "Sienkiewicza 3A", "50.088020",
					"18.219696", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Raciborzu", "(32)4181592",
					"sekretariat@rckik.pl", "http://rckik.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Radom", "26-600", "Limanowskiego 42", "51.397985",
					"21.137517", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa im. dra Konrada Vietha w Radomiu",
					"(48)3626276", "rckik@rckik.radom.pl", "http://www.rckik.radom.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Rzeszów", "35-310", "Wierzbowa 14", "50.033511",
					"22.015251", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Rzeszowie", "(17)8672030",
					"sekretariat@rckk.rzeszow.pl", "http://www.rckk.rzeszow.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Słupsk", "76-200", "Szarych Szeregów 21", "54.469767",
					"17.032606", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa im. Jana Pawła II w Słupsku",
					"(59)8422021", "sekretariat@krwiodawstwo.slupsk.pl", "http://www.krwiodawstwo.slupsk.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Wałbrzych", "58-300", "Bolesława Chrobrego 31",
					"50.774143", "16.274953", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Wałbrzychu",
					"(74)6646319", "sekretariat@rckik.walbrzych.pl", "http://www.rckik.walbrzych.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Warszawa", "03-948", "Saska 63/75", "52.232719",
					"21.059888", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Warszawie", "(22)5146060",
					"rckik@rckik-warszawa.com.pl", "http://www.rckik-warszawa.com.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Wrocław", "50-345", "Czerwonego Krzyża 5/9", "51.115976",
					"17.065823",
					"Regionalne Centrum Krwiodawstwa i Krwiolecznictwa im. prof dr hab. Tadeusza Dorobisza we Wrocławiu",
					"(71)3715812", "sekretariat@rckik.wroclaw.pl", "http://www.rckik.wroclaw.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Zielona Góra", "65-046", "Zyty 21", "51.940525",
					"15.518936", "Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Zielonej Górze", "(68)3298360",
					"dyrekcja@rckik.zgora.pl", "http://www.rckik.zgora.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Warszawa", "00-671", "Koszykowa 78", "52.223128",
					"21.005526", "Wojskowe Centrum Krwiodawstwa i Krwiolecznictwa", "261845066", "sekretariat@wckik.pl",
					"http://www.wckik.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Warszawa", "02-507", "Wołoska 137", "52.198666",
					"20.996756",
					"Centrum Krwiodawstwa i Krwiolecznictwa Ministerstwa Spraw Wewnętrznych i Administracji",
					"(22)5081312", "info@ckikmsw.pl", "http://ckikmsw.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Warszawa", "02-776", "Indiry Gandhi 14", "52.145505",
					"21.033019", "Instytut Hematologii i Transfuzjologii", "(22)3496100", "sekihit@ihit.waw.pl",
					"http://www.ihit.waw.pl", roles);
			saveToCenterAndAddressAndCenterUserRepositories("Warszawa", "00-080", "Miodowa 1", "52.245977", "21.013199",
					"Narodowe Centrum Krwi", "(22)5564900", "nck@nck.gov.pl", "http://www.gov.pl/web/nck", roles);
		}
	}

	private void saveToBloodRepository(String bloodGroup, String rh) {

		long idBloodInDb = 0;
		String bloodId = randomStringGenerator.generateBloodId(30);
		List<BloodResourceEntity> bloodResourcesList = null;
		List<AlertEntity> listOfAlertEntitiesIsNull = null;
		bloodRepository.save(
				new BloodEntity(idBloodInDb, bloodId, bloodGroup, rh, bloodResourcesList, listOfAlertEntitiesIsNull));
	}

	private void saveToCenterAndAddressAndCenterUserRepositories(String city, String postalCode, String street,
			String longitude, String latitude, String centerName, String phoneNumber, String email, String webSite,
			Collection<RoleEntity> roles) {

		long idAddress = 0;
		long idCenter = 0;
		long idUser = 0;

		String stringAddressId = randomStringGenerator.generateAddressId(30);
		String stringCenterId = randomStringGenerator.generateCenterId(30);
		String stringCenterUserId = randomStringGenerator.generateCenterUserId(30);

		CenterEntity centerEntityIsNull = null;
		CenterUserEntity centerUserEntityIsNull = null;
		List<BloodResourceEntity> listOfBloodResourceEntities = new ArrayList<>();
		Iterable<BloodEntity> listOfBloodsFromDb = bloodRepository.findAll();

		for (BloodEntity bloodEntity : listOfBloodsFromDb) {

			BloodResourceEntity bloodResourceEntity = new BloodResourceEntity();

			bloodResourceEntity.setBloodResourceId(randomStringGenerator.generateBloodResourceId(30));
			bloodResourceEntity.setBlood(bloodEntity);
			bloodResourceEntity.setVolume(0);
			bloodResourceRepository.save(bloodResourceEntity);
			listOfBloodResourceEntities.add(bloodResourceEntity);

		}

		List<AlertEntity> listOfAlertEntitiesIsNull = null;

		AddressEntity addressEntity = new AddressEntity(idAddress, stringAddressId, city, postalCode, street, longitude,
				latitude, centerEntityIsNull);

		CenterEntity centerEntity = new CenterEntity(idCenter, stringCenterId, centerName, phoneNumber, email, webSite,
				addressEntity, listOfBloodResourceEntities, centerUserEntityIsNull);

		String centerUserLogin = centerEntity.getEmail();
		String password = randomStringGenerator.generateInitialPasswordForCenterUsers(30);
		String encryptedPassword = bCryptPasswordEncoder.encode(password);

		try (PrintWriter out = new PrintWriter(new FileOutputStream("centerusers.txt", true))) {

			out.println(centerUserLogin);
			out.println(password);
			out.println("");

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		CenterUserEntity centerUserEntity = new CenterUserEntity(idUser, stringCenterUserId, centerUserLogin,
				encryptedPassword, centerEntity, listOfAlertEntitiesIsNull, roles);

		addressEntity.setCenter(centerEntity);
		centerRepository.save(centerEntity);

		for (BloodResourceEntity createdBloodResourceEntity : listOfBloodResourceEntities) {

			createdBloodResourceEntity.setCenter(centerEntity);

			bloodResourceRepository.save(createdBloodResourceEntity);
		}

		centerUserRepository.save(centerUserEntity);

	}

}
