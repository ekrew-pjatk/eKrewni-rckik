package com.ekrewni.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;

@Repository
public interface CenterUserRepository extends PagingAndSortingRepository<CenterUserEntity, Long> {

	CenterUserEntity findByLogin(String login);

	CenterUserEntity findByUserId(String userId);

	CenterUserEntity findByCenter(CenterEntity centerEntity);

}
