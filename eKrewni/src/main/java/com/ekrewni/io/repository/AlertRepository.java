package com.ekrewni.io.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.AlertEntity;
import com.ekrewni.io.entity.CenterUserEntity;

@Repository
public interface AlertRepository extends PagingAndSortingRepository<AlertEntity, Long> {

	AlertEntity findByAlertId(String alertId);

	List<AlertEntity> findByCenterUser(CenterUserEntity centerUserEntity);
	
	@Query("select alert from AlertEntity alert where alert.centerUser = :centerUser")
	Page<AlertEntity> findAlertsByCenterUser(@Param("centerUser") CenterUserEntity centerUser, Pageable pageableRequest);

	@Query("select alert from AlertEntity alert where alert.centerUser.center.name in :centers and " +
			"concat(alert.blood.bloodType, alert.blood.rh) in :groups order by alert.date DESC")
	Page<AlertEntity> findAllByFilter(
			@Param("groups") List<String> groups,
			@Param("centers") List<String> centers,
			Pageable pageableRequest
	);

	@Query("select alert from AlertEntity alert order by alert.date DESC")
	Page<AlertEntity> findAllSortedByDate(Pageable pageableRequest);
}
