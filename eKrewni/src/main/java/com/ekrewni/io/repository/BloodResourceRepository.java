package com.ekrewni.io.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;

public interface BloodResourceRepository extends PagingAndSortingRepository<BloodResourceEntity, Long> {

	BloodResourceEntity findByBloodResourceId(String bloodResourceId);

	List<BloodResourceEntity> findAllByCenter(CenterEntity centerEntity);
}
