package com.ekrewni.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.BloodEntity;

@Repository
public interface BloodRepository extends PagingAndSortingRepository<BloodEntity, Long> {

	BloodEntity findByBloodType(String bloodType);

	BloodEntity findByRh(String rh);

	BloodEntity findByBloodTypeAndRh(String bloodType, String rh);

	BloodEntity findByBloodId(String bloodId);
}
