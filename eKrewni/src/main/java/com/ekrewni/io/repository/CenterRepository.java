package com.ekrewni.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;

@Repository
public interface CenterRepository extends PagingAndSortingRepository<CenterEntity, Long> {

	CenterEntity findByCenterId(String centerId);

	CenterEntity findByCenterUser(CenterUserEntity centerUserEntity);
	
}
