package com.ekrewni.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.AddressEntity;
import com.ekrewni.io.entity.CenterEntity;

@Repository
public interface AddressRepository extends PagingAndSortingRepository<AddressEntity, Long> {

	AddressEntity findByCenter(CenterEntity centerEntity);

	AddressEntity findByAddressId(String addressId);
}
