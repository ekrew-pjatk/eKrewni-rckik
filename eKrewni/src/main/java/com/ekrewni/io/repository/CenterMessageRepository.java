package com.ekrewni.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ekrewni.io.entity.CenterMessageEntity;

@Repository
public interface CenterMessageRepository extends PagingAndSortingRepository<CenterMessageEntity, Long> {

	CenterMessageEntity findByCenterMessageId(String centerMessageId);
}
