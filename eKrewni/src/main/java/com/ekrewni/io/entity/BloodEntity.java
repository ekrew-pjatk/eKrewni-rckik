package com.ekrewni.io.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "bloodgroups")
public class BloodEntity implements Serializable {

	private static final long serialVersionUID = -2027151759593465932L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String bloodId;

	@Column(nullable = false, length = 2)
	private String bloodType;

	@Column(nullable = false, length = 1)
	private String rh;

	@OneToMany(mappedBy = "blood")
	private List<BloodResourceEntity> bloodResources;

	@OneToMany(mappedBy = "blood")
	private List<AlertEntity> alerts;

	public BloodEntity() {

	}

	public BloodEntity(long id, String bloodId, String bloodType, String rh, List<BloodResourceEntity> bloodResources,
			List<AlertEntity> alerts) {

		super();
		this.id = id;
		this.bloodId = bloodId;
		this.bloodType = bloodType;
		this.rh = rh;
		this.bloodResources = bloodResources;
		this.alerts = alerts;
	}

	public List<BloodResourceEntity> getBloodResources() {

		return bloodResources;
	}

	public void setBloodResources(List<BloodResourceEntity> bloodResources) {

		this.bloodResources = bloodResources;
	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getBloodId() {

		return bloodId;
	}

	public void setBloodId(String bloodId) {

		this.bloodId = bloodId;
	}

	public String getBloodType() {

		return bloodType;
	}

	public void setBloodType(String bloodType) {

		this.bloodType = bloodType;
	}

	public String getRh() {

		return rh;
	}

	public void setRh(String rh) {

		this.rh = rh;
	}

	public List<AlertEntity> getAlerts() {

		return alerts;
	}

	public void setAlerts(List<AlertEntity> alerts) {

		this.alerts = alerts;
	}

}
