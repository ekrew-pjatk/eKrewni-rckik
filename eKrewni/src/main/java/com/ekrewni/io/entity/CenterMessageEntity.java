package com.ekrewni.io.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "centermessages")
public class CenterMessageEntity implements Serializable {

	private static final long serialVersionUID = -6207397622665126390L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String centerMessageId;

	@Column(nullable = false, length = 1000000)
	private String text;

	private LocalDateTime dateTime;

	@ManyToOne
	@JoinColumn(name = "centerusers_id")
	private CenterUserEntity centerUser;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getCenterMessageId() {

		return centerMessageId;
	}

	public void setCenterMessageId(String centerMessageId) {

		this.centerMessageId = centerMessageId;
	}

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}

	public LocalDateTime getDateTime() {

		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {

		this.dateTime = dateTime;
	}

	public CenterUserEntity getCenterUser() {

		return centerUser;
	}

	public void setCenterUser(CenterUserEntity centerUser) {

		this.centerUser = centerUser;
	}

}
