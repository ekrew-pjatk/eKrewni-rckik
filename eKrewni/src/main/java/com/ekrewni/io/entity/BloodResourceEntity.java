package com.ekrewni.io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "bloodresources")
public class BloodResourceEntity implements Serializable {

	private static final long serialVersionUID = 1517954841136453007L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, length = 250)
	private String bloodResourceId;

	@Column(nullable = false)
	private double volume;

	@ManyToOne
	@JoinColumn(name = "bloodgroups_id")
	private BloodEntity blood;

	@ManyToOne
	@JoinColumn(name = "centers_id")
	private CenterEntity center;

	public BloodResourceEntity() {

	}

	public BloodResourceEntity(long id, String bloodResourceId, double volume, BloodEntity bloodEntity,
			CenterEntity centerEntity) {

		super();
		this.id = id;
		this.bloodResourceId = bloodResourceId;
		this.volume = volume;
		this.blood = bloodEntity;
		this.center = centerEntity;
	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getBloodResourceId() {

		return bloodResourceId;
	}

	public void setBloodResourceId(String bloodResourceId) {

		this.bloodResourceId = bloodResourceId;
	}

	public double getVolume() {

		return volume;
	}

	public void setVolume(double volume) {

		this.volume = volume;
	}

	public BloodEntity getBlood() {

		return blood;
	}

	public void setBlood(BloodEntity blood) {

		this.blood = blood;
	}

	public CenterEntity getCenter() {

		return center;
	}

	public void setCenter(CenterEntity center) {

		this.center = center;
	}

}
