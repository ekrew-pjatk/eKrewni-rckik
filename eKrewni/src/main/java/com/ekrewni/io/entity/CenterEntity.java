package com.ekrewni.io.entity;

import java.io.Serializable;
//import java.util.List;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "centers")
public class CenterEntity implements Serializable {

	private static final long serialVersionUID = 5823326752559158211L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String centerId;

	@Column(nullable = false, length = 250)
	private String name;

	@Column(nullable = false, length = 50)
	private String phoneNumber;

	@Column(nullable = false, length = 100)
	private String email;

	@Column(nullable = false, length = 100)
	private String webSite;

	@OneToOne(mappedBy = "center", cascade = CascadeType.ALL)
	private AddressEntity address;

	@OneToMany(mappedBy = "center")
	private List<BloodResourceEntity> bloodResources;

	@OneToOne(mappedBy = "center", cascade = CascadeType.ALL)
	private CenterUserEntity centerUser;

	public CenterEntity() {

	}

	public CenterEntity(long id, String centerId, String name, String phoneNumber, String email, String webSite,
			AddressEntity address, List<BloodResourceEntity> bloodResources, CenterUserEntity centerUser) {

		super();
		this.id = id;
		this.centerId = centerId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.webSite = webSite;
		this.address = address;
		this.bloodResources = bloodResources;
		this.centerUser = centerUser;

	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getCenterId() {

		return centerId;
	}

	public void setCenterId(String centerId) {

		this.centerId = centerId;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getPhoneNumber() {

		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getWebSite() {

		return webSite;
	}

	public void setWebSite(String webSite) {

		this.webSite = webSite;
	}

	public AddressEntity getAddress() {

		return address;
	}

	public void setAddress(AddressEntity address) {

		this.address = address;
	}

	public List<BloodResourceEntity> getBloodResources() {

		return bloodResources;
	}

	public void setBloodResources(List<BloodResourceEntity> bloodResources) {

		this.bloodResources = bloodResources;
	}

	public CenterUserEntity getCenterUser() {

		return centerUser;
	}

	public void setCenterUser(CenterUserEntity centerUser) {

		this.centerUser = centerUser;
	}

}
