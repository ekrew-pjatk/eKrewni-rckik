package com.ekrewni.io.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "alerts")
public class AlertEntity implements Serializable {

	private static final long serialVersionUID = 3596795720574355767L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String alertId;

	@Column(length = 500)
	private String message;

	@Column(length = 50, nullable = false)
	private LocalDateTime date;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bloodgroups_id")
	private BloodEntity blood;

	@ManyToOne
	@JoinColumn(name = "centerusers_id")
	private CenterUserEntity centerUser;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getAlertId() {

		return alertId;
	}

	public void setAlertId(String alertId) {

		this.alertId = alertId;
	}

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = message;
	}

	public LocalDateTime getDate() {

		return date;
	}

	public void setDate(LocalDateTime date) {

		this.date = date;
	}

	public BloodEntity getBlood() {

		return blood;
	}

	public void setBlood(BloodEntity blood) {

		this.blood = blood;
	}

	public CenterUserEntity getCenterUser() {

		return centerUser;
	}

	public void setCenterUser(CenterUserEntity centerUser) {

		this.centerUser = centerUser;
	}

}
