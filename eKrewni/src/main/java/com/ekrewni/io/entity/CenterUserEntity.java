package com.ekrewni.io.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity(name = "centerusers")
public class CenterUserEntity implements Serializable {

	private static final long serialVersionUID = -3929870086370523977L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String userId;

	@Column(nullable = false, length = 100, unique = true)
	private String login;

	@Column(nullable = false)
	private String encryptedPassword;

	@OneToOne
	@JoinColumn(name = "centers_id")
	private CenterEntity center;

	@OneToMany(mappedBy = "centerUser")
	private List<CenterMessageEntity> centerMessages;

	@OneToMany(mappedBy = "centerUser")
	private List<AlertEntity> alerts;

	@ManyToMany(cascade = { CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "centerusers_roles", joinColumns = @JoinColumn(name = "centerusers_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "roles_id", referencedColumnName = "id"))
	private Collection<RoleEntity> roles;

	public CenterUserEntity() {

	}

	public CenterUserEntity(long userId, String centerUserId, String centerUserLogin, String encryptedPassword,
			CenterEntity centerEntity, List<AlertEntity> alerts, Collection<RoleEntity> roles) {

		super();
		this.id = userId;
		this.userId = centerUserId;
		this.login = centerUserLogin;
		this.encryptedPassword = encryptedPassword;
		this.center = centerEntity;
		this.alerts = alerts;
		this.roles = roles;
	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getUserId() {

		return userId;
	}

	public void setUserId(String userId) {

		this.userId = userId;
	}

	public String getLogin() {

		return login;
	}

	public void setLogin(String login) {

		this.login = login;
	}

	public String getEncryptedPassword() {

		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {

		this.encryptedPassword = encryptedPassword;
	}

	public CenterEntity getCenter() {

		return center;
	}

	public void setCenter(CenterEntity center) {

		this.center = center;
	}

	public List<CenterMessageEntity> getCenterMessages() {

		return centerMessages;
	}

	public void setCenterMessages(List<CenterMessageEntity> centerMessages) {

		this.centerMessages = centerMessages;
	}

	public List<AlertEntity> getAlerts() {

		return alerts;
	}

	public void setAlerts(List<AlertEntity> alerts) {

		this.alerts = alerts;
	}

	public Collection<RoleEntity> getRoles() {

		return roles;
	}

	public void setRoles(Collection<RoleEntity> roles) {

		this.roles = roles;
	}
}
