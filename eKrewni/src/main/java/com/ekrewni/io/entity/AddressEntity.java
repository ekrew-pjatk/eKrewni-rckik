package com.ekrewni.io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "addresses")
public class AddressEntity implements Serializable {

	private static final long serialVersionUID = 9026526363572038890L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String addressId;

	@Column(nullable = false, length = 50)
	private String city;

	@Column(nullable = false, length = 10)
	private String postalCode;

	@Column(nullable = false, length = 100)
	private String street;

	@Column(nullable = false, length = 9)
	private String latitude;

	@Column(nullable = false, length = 9)
	private String longitude;

	@OneToOne
	@JoinColumn(name = "centers_id")
	private CenterEntity center;

	public AddressEntity() {

	}

	public AddressEntity(long id, String addressId, String city, String postalCode, String street, String latitude,
			String longitude, CenterEntity center) {

		super();
		this.id = id;
		this.addressId = addressId;
		this.city = city;
		this.postalCode = postalCode;
		this.street = street;
		this.latitude = latitude;
		this.longitude = longitude;
		this.center = center;
	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getAddressId() {

		return addressId;
	}

	public void setAddressId(String addressId) {

		this.addressId = addressId;
	}

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getPostalCode() {

		return postalCode;
	}

	public void setPostalCode(String postalCode) {

		this.postalCode = postalCode;
	}

	public String getStreet() {

		return street;
	}

	public void setStreet(String street) {

		this.street = street;
	}

	public String getLatitude() {

		return latitude;
	}

	public void setLatitude(String latitude) {

		this.latitude = latitude;
	}

	public String getLongitude() {

		return longitude;
	}

	public void setLongitude(String longitude) {

		this.longitude = longitude;
	}

	public CenterEntity getCenter() {

		return center;
	}

	public void setCenter(CenterEntity center) {

		this.center = center;
	}

}
