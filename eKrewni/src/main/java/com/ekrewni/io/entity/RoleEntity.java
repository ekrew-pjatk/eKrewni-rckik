package com.ekrewni.io.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity(name = "roles")
public class RoleEntity implements Serializable {

	private static final long serialVersionUID = 1497637228185703358L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, length = 20)
	private String name;

	@ManyToMany(mappedBy = "roles")
	private Collection<CenterUserEntity> centerUsers;

	@ManyToMany(cascade = { CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "roles_authorities", joinColumns = @JoinColumn(name = "roles_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "authorities_id", referencedColumnName = "id"))
	private Collection<AuthorityEntity> authorities;

	public RoleEntity() {

	}

	public RoleEntity(String name) {

		this.name = name;
	}

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public Collection<CenterUserEntity> getCenterUsers() {

		return centerUsers;
	}

	public void setCenterUsers(Collection<CenterUserEntity> centerUsers) {

		this.centerUsers = centerUsers;
	}

	public Collection<AuthorityEntity> getAuthorities() {

		return authorities;
	}

	public void setAuthorities(Collection<AuthorityEntity> authorities) {

		this.authorities = authorities;
	}
}
