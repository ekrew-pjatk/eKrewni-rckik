package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.AddressDto;

public interface AddressService {

	AddressDto getAddressByCenterId(String centerId);

	AddressDto updateAddress(String addressId, AddressDto addressDto) throws NameNotFoundException;

	List<AddressDto> getAddresses(int numberOfPage, int maxPages);

}
