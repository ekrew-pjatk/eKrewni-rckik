package com.ekrewni.service.implementation;

import java.time.LocalDateTime;
import java.util.ArrayList;
//import java.util.ArrayList;
//import java.util.List;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
//import org.modelmapper.config.Configuration.AccessLevel;
//import org.modelmapper.convention.NamingConventions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

//import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterMessageEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.CenterMessageRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.service.CenterMessageService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.CenterMessageDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class CenterMessageServiceImpl implements CenterMessageService {

	@Autowired
	CenterRepository centerRepository;

	@Autowired
	CenterMessageRepository centerMessageRepository;
	
	@Autowired
	CenterUserRepository centerUserRepository;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Override
	public CenterMessageDto createMessage(CenterMessageDto messageDto) {

		if (centerMessageRepository.findByCenterMessageId(messageDto.getCenterMessageId()) != null) {

			throw new RuntimeException("Record already exist");

		}

		ModelMapper modelMapper = new ModelMapper();
		CenterMessageEntity centerMessageEntity = modelMapper.map(messageDto, CenterMessageEntity.class);

		String publicCenterMessageId = randomStringGenerator.generateCenterMessageId(30);
		centerMessageEntity.setCenterMessageId(publicCenterMessageId);

		LocalDateTime timeOfCreation = LocalDateTime.now(); // setting time of creation the message
		centerMessageEntity.setDateTime(timeOfCreation);
		
		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(messageDto.wezCenterUserLogin());
		centerMessageEntity.setCenterUser(centerUserEntity);
		
		CenterMessageEntity storedCenterMessage = centerMessageRepository.save(centerMessageEntity);
		CenterMessageDto createdCenterMessageDto = modelMapper.map(storedCenterMessage, CenterMessageDto.class);
		
		return createdCenterMessageDto;
	}

	@Override
	public CenterMessageDto getCenterMessageByMessageId(String messageId) {

		CenterMessageDto centerMessageDto = new CenterMessageDto();
		CenterMessageEntity centerMessageEntity = centerMessageRepository.findByCenterMessageId(messageId);

		if (centerMessageEntity == null) {

			throw new UsernameNotFoundException("Message with ID: " + messageId + " not found");
		}

		ModelMapper modelMapper = new ModelMapper();
		centerMessageDto = modelMapper.map(centerMessageEntity, CenterMessageDto.class);

		return centerMessageDto;
	}

	@Override
	public CenterMessageDto updateCenterMessage(String messageId, CenterMessageDto messageDto) throws NameNotFoundException {

		CenterMessageDto updatedCenterMessageDto = new CenterMessageDto();
		CenterMessageEntity centerMessageEntity = centerMessageRepository.findByCenterMessageId(messageId);

		if (centerMessageEntity == null) {

			throw new NameNotFoundException("Message with ID: " + messageId + " not found");
		}

		centerMessageEntity.setText(messageDto.getText());
		CenterMessageEntity updatedCenterMessage = centerMessageRepository.save(centerMessageEntity);
		ModelMapper modelMapper = new ModelMapper();
		updatedCenterMessageDto = modelMapper.map(updatedCenterMessage, CenterMessageDto.class);

		return updatedCenterMessageDto;
	}

	@Override
	public void deleteCenterMessage(String messageId) throws Exception {

		CenterMessageEntity centerMessageEntity = centerMessageRepository.findByCenterMessageId(messageId);

		if (centerMessageEntity == null) {

			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}

		centerMessageRepository.delete(centerMessageEntity);
	}

	@Override
	public List<CenterMessageDto> getCentersMessages(int numberOfpage, int limitOfPages) {

		List<CenterMessageDto> listOfCentersMessagesDto = new ArrayList<>();

		if (numberOfpage > 0) {

			numberOfpage = numberOfpage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfpage, limitOfPages);

		Page<CenterMessageEntity> centersMessagesPage = centerMessageRepository.findAll(pageableRequest);

		List<CenterMessageEntity> centersMessages = centersMessagesPage.getContent();

		for (CenterMessageEntity centersMessagesEntity : centersMessages) {

			CenterMessageDto centersMessagesDto = new CenterMessageDto();
			ModelMapper modelMapper = new ModelMapper();
			centersMessagesDto = modelMapper.map(centersMessagesEntity, CenterMessageDto.class);

			listOfCentersMessagesDto.add(centersMessagesDto);
		}

		return listOfCentersMessagesDto;
	}

}
