package com.ekrewni.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.io.repository.BloodResourceRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.service.CenterService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.AddressDto;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.shared.dto.CenterDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class CenterServiceImpl implements CenterService {

	@Autowired
	CenterRepository centerRepository;
	
    @Autowired
    BloodRepository bloodRepository;

    @Autowired
    BloodResourceRepository bloodResourceRepository;
    
	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Override
	public CenterDto createCenter(CenterDto centerDto) {

		ModelMapper modelMapper = new ModelMapper();
		String publicCenterId = randomStringGenerator.generateCenterId(30);
		
		if (centerRepository.findByCenterId(centerDto.getCenterId()) != null) {

			throw new RuntimeException("Record already exists");
		}

		AddressDto address = centerDto.getAddress();
		address.setCenter(centerDto);
		address.setAddressId(randomStringGenerator.generateAddressId(30));
		centerDto.setAddress(address);
		centerDto.setCenterId(publicCenterId);
		
		Iterable<BloodEntity> listOfBloodsFromDb = bloodRepository.findAll();
		List<BloodResourceEntity> listOfBloodResourceEntities = new ArrayList<>();
		
		CenterEntity centerEntityForMap = modelMapper.map(centerDto, CenterEntity.class);
		
		for (BloodEntity bloodEntity : listOfBloodsFromDb) {

			BloodResourceEntity bloodResourceEntity = new BloodResourceEntity();
			bloodResourceEntity.setBloodResourceId(randomStringGenerator.generateBloodResourceId(30));
			bloodResourceEntity.setBlood(bloodEntity);
			bloodResourceEntity.setVolume(0);
			bloodResourceEntity.setCenter(centerEntityForMap);
			listOfBloodResourceEntities.add(bloodResourceEntity);

		}

		List<BloodResourceDto> listOfBloodResourceDtos = new ArrayList<>();
		
		for (BloodResourceEntity bloodResourceEntity : listOfBloodResourceEntities) {
			
			BloodResourceDto bloodResourceDto = new BloodResourceDto();
			bloodResourceDto = modelMapper.map(bloodResourceEntity, BloodResourceDto.class);
			listOfBloodResourceDtos.add(bloodResourceDto);
		}

		centerDto.setBloodResources(listOfBloodResourceDtos);
		
		CenterEntity centerEntity = modelMapper.map(centerDto, CenterEntity.class);

		CenterEntity storedCenterDetails = centerRepository.save(centerEntity);
		
		for (BloodResourceEntity bloodResourceEntity : listOfBloodResourceEntities) {
			
			bloodResourceEntity.setCenter(centerEntity);
			bloodResourceRepository.save(bloodResourceEntity);
		}

		CenterDto createdCenterDto = modelMapper.map(storedCenterDetails, CenterDto.class);

		return createdCenterDto;
	}

	@Override
	public CenterDto getCenterByCenterId(String centerId) {

		CenterDto centerDto = new CenterDto();
		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {

			throw new UsernameNotFoundException("Center with ID: " + centerId + " not found");
		}

		ModelMapper modelMapper = new ModelMapper();
		centerDto = modelMapper.map(centerEntity, CenterDto.class);

		return centerDto;
	}

	@Override
	public CenterDto updateCenter(String centerId, CenterDto centerDto) throws NameNotFoundException {

		CenterDto updatedCenterDto = new CenterDto();
		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {

			throw new NameNotFoundException("Center with ID: " + centerId + " not found");
		}

		centerEntity.setName(centerDto.getName());
		centerEntity.setEmail(centerDto.getEmail());
		centerEntity.setPhoneNumber(centerDto.getPhoneNumber());
		centerEntity.setWebSite(centerDto.getWebSite());

		CenterEntity updatedCenter = centerRepository.save(centerEntity);

		ModelMapper modelMapper = new ModelMapper();
		updatedCenterDto = modelMapper.map(updatedCenter, CenterDto.class);

		return updatedCenterDto;
	}

	@Override
	public void deleteCenter(String centerId) throws Exception {

		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {

			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}

		centerRepository.delete(centerEntity);
	}

	@Override
	public List<CenterDto> getCenters(int numberOfPage, int maxPages) {
		
		List<CenterDto> centerDtos = new ArrayList<>();
		
		if (numberOfPage > 0) {

			numberOfPage = numberOfPage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);

		Page<CenterEntity> centersPage = centerRepository.findAll(pageableRequest);
		List<CenterEntity> centerEntities = centersPage.getContent();

		for (CenterEntity centerEntity : centerEntities) {

			CenterDto centerDto = new CenterDto();
			ModelMapper modelMapper = new ModelMapper();
			centerDto = modelMapper.map(centerEntity, CenterDto.class);
			centerDtos.add(centerDto);
		}

		return centerDtos;
	}

}

