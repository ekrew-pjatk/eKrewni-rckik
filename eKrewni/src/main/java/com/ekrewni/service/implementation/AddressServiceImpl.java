package com.ekrewni.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ekrewni.io.entity.AddressEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.repository.AddressRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.service.AddressService;
import com.ekrewni.shared.dto.AddressDto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	CenterRepository centerRepository;

	@Autowired
	AddressRepository addressRepository;

	@Override
	public AddressDto getAddressByCenterId(String centerId) {

		AddressDto addressDto = new AddressDto();
		ModelMapper modelMapper = new ModelMapper();

		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {
			
			return addressDto;
		}

		AddressEntity addressEntity = addressRepository.findByCenter(centerEntity);
		
		if (addressEntity == null) {

			throw new UsernameNotFoundException("Address of Center: " + centerEntity.getName() + " not found");
		}
		
		addressDto = modelMapper.map(addressEntity, AddressDto.class);

		return addressDto;
	}

	@Override
	public AddressDto updateAddress(String addressId, AddressDto addressDto) throws NameNotFoundException {

		AddressDto updatedAddressDto = new AddressDto();
		AddressEntity addressEntity = addressRepository.findByAddressId(addressId);

		if (addressEntity == null) {

			throw new NameNotFoundException("Address with ID: " + addressId + " not found");
		}

		addressEntity.setCity(addressDto.getCity());
		addressEntity.setPostalCode(addressDto.getPostalCode());
		addressEntity.setStreet(addressDto.getStreet());
		addressEntity.setLatitude(addressDto.getLatitude());
		addressEntity.setLongitude(addressDto.getLongitude());

		AddressEntity updatedAddress = addressRepository.save(addressEntity);

		ModelMapper modelMapper = new ModelMapper();
		updatedAddressDto = modelMapper.map(updatedAddress, AddressDto.class);

		return updatedAddressDto;
	}

	@Override
	public List<AddressDto> getAddresses(int numberOfPage, int maxPages) {

		List<AddressDto> listOfAddressDto = new ArrayList<>();
		if (numberOfPage > 0) {

			numberOfPage = numberOfPage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);
		Page<AddressEntity> addressPage = addressRepository.findAll(pageableRequest);
		List<AddressEntity> addresses = addressPage.getContent();
		for(AddressEntity addressEntity: addresses) {
			
			AddressDto addressDto = new AddressDto();
			ModelMapper modelMapper = new ModelMapper();
			addressDto = modelMapper.map(addressEntity, AddressDto.class);
			listOfAddressDto.add(addressDto);
			
		}
		return listOfAddressDto;
	}
}
