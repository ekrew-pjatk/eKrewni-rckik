package com.ekrewni.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.service.BloodService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.BloodDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class BloodServiceImpl implements BloodService {

	@Autowired
	BloodRepository bloodRepository;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Override
	public BloodDto createBloodGroup(BloodDto bloodDto) {

		BloodEntity storedBloodtypeAndRh = bloodRepository.findByBloodTypeAndRh(bloodDto.getBloodType(),
				bloodDto.getRh());

		if (storedBloodtypeAndRh != null) {

			throw new CenterUserServiceException("Record already exist");

		}

		ModelMapper modelMapper = new ModelMapper();
		BloodEntity bloodEntity = modelMapper.map(bloodDto, BloodEntity.class);

		String publicBloodId = randomStringGenerator.generateBloodId(30);
		bloodEntity.setBloodId(publicBloodId);

		BloodEntity storedBloodDetails = bloodRepository.save(bloodEntity);
		BloodDto createdBloodDto = modelMapper.map(storedBloodDetails, BloodDto.class);

		return createdBloodDto;

	}

	@Override
	public BloodDto getBloodByBloodType(String bloodType) {

		BloodEntity bloodEntity = bloodRepository.findByBloodType(bloodType);

		if (bloodEntity == null) {

			throw new UsernameNotFoundException("Blood with type: " + bloodType + " not found.");
		}

		BloodDto bloodDto = new BloodDto();
		ModelMapper modelMapper = new ModelMapper();
		bloodDto = modelMapper.map(bloodEntity, BloodDto.class);
		
		return bloodDto;
	}

	@Override
	public BloodDto getBloodByRh(String rh) {

		BloodEntity bloodEntity = bloodRepository.findByRh(rh);

		if (bloodEntity == null) {

			throw new UsernameNotFoundException("Blood with rh: " + rh + " not found.");
		}

		BloodDto bloodDto = new BloodDto();
		ModelMapper modelMapper = new ModelMapper();
		bloodDto = modelMapper.map(bloodEntity, BloodDto.class);

		return bloodDto;
	}

	@Override
	public BloodDto getBloodByBloodId(String bloodId) {

		BloodDto bloodDtoById = new BloodDto();
		BloodEntity bloodEntity = bloodRepository.findByBloodId(bloodId);

		if (bloodEntity == null) {

			throw new UsernameNotFoundException("Blood with ID:" + bloodId + " not found.");
		}

		ModelMapper modelMapper = new ModelMapper();
		bloodDtoById = modelMapper.map(bloodEntity, BloodDto.class);

		return bloodDtoById;
	}

	@Override
	public BloodDto updateBlood(String bloodId, BloodDto bloodDto) throws NameNotFoundException {

		BloodDto updatedBloodDto = new BloodDto();
		BloodEntity bloodEntity = bloodRepository.findByBloodId(bloodId);
		
		if(bloodEntity == null) {
			
			throw new NameNotFoundException("Blood with ID:" + bloodId + " not found.");
		}
		
		bloodEntity.setBloodType(bloodDto.getBloodType());
		bloodEntity.setRh(bloodDto.getRh());
		
		BloodEntity updatedBloodEntity = bloodRepository.save(bloodEntity);
		ModelMapper modelMapper = new ModelMapper();
		updatedBloodDto = modelMapper.map(updatedBloodEntity, BloodDto.class);
		
		return updatedBloodDto;
	}

	@Override
	public void deleteBlood(String bloodId) throws Exception {

		BloodEntity bloodEntity = bloodRepository.findByBloodId(bloodId);
		
		if(bloodEntity == null) {
			
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}
		
		bloodRepository.delete(bloodEntity);
	}

	@Override
	public List<BloodDto> getBloodGroups(int numberOfpage, int limitOfPages) {

		List<BloodDto> listOfBloodDto = new ArrayList<>();

		if (numberOfpage > 0) {

			numberOfpage = numberOfpage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfpage, limitOfPages);

		Page<BloodEntity> bloodssPage = bloodRepository.findAll(pageableRequest);

		List<BloodEntity> bloods = bloodssPage.getContent();

		for (BloodEntity bloodEntity : bloods) {

			BloodDto bloodDto = new BloodDto();
			ModelMapper modelMapper = new ModelMapper();
			bloodDto = modelMapper.map(bloodEntity, BloodDto.class);

			listOfBloodDto.add(bloodDto);
		}

		return listOfBloodDto;
	}

}