package com.ekrewni.service.implementation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.AlertEntity;
import com.ekrewni.io.entity.BloodEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.AlertRepository;
import com.ekrewni.io.repository.BloodRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.service.AlertService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class AlertServiceImpl implements AlertService {

	@Autowired
	AlertRepository alertRepository;

	@Autowired
	BloodRepository bloodRepository;

	@Autowired
	CenterRepository centerRepository;

	@Autowired
	CenterUserRepository centerUserRepository;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Override
	public AlertDto createAlert(AlertDto alert) {

		if (alertRepository.findByAlertId(alert.getAlertId()) != null) {

			throw new RuntimeException("Record already exists");
		}

		ModelMapper modelMapper = new ModelMapper();
		AlertEntity alertEntity = modelMapper.map(alert, AlertEntity.class);

		String publicAlertId = randomStringGenerator.generateAlertId(30);
		alertEntity.setAlertId(publicAlertId);
		LocalDateTime timeOfCreation = LocalDateTime.now(); // setting time of creation the alert
		alertEntity.setDate(timeOfCreation);

		/*
		 * USTAWIANIE UŻYTKOWNIKA PO LOGINIE
		 */
		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(alert.pobierzCenterUserLogin());
		alertEntity.setCenterUser(centerUserEntity);
		String bloodType = alertEntity.getBlood().getBloodType();
		String rh = alertEntity.getBlood().getRh();
		BloodEntity blood = bloodRepository.findByBloodTypeAndRh(bloodType, rh);
		alertEntity.setBlood(blood);
		AlertEntity storedAlertDetails = alertRepository.save(alertEntity);
		
		AlertDto alertDto = modelMapper.map(storedAlertDetails, AlertDto.class);

		return alertDto;
	}

	@Override
	public AlertDto getAlertByAlertId(String alertId) {

		AlertDto alertDto = new AlertDto();
		AlertEntity alertEntity = alertRepository.findByAlertId(alertId);

		if (alertEntity == null) {

			throw new UsernameNotFoundException("Alert with ID: " + alertId + " not found");
		}

		ModelMapper modelMapper = new ModelMapper();
		alertDto = modelMapper.map(alertEntity, AlertDto.class);

		return alertDto;
	}

	@Override
	public AlertDto updateAlert(String alertId, AlertDto alertDto) throws NameNotFoundException {

		AlertDto updatedAlertDto = new AlertDto();
		AlertEntity alertEntity = alertRepository.findByAlertId(alertId);

		if (alertEntity == null) {

			throw new NameNotFoundException("Alert with ID: " + alertId + " not found");
		}

		String userIdOfAlert = alertEntity.getCenterUser().getUserId();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(currentPrincipalName);
		String userIdOfLoggedInUser = centerUserEntity.getUserId();
		
		if(userIdOfAlert == userIdOfLoggedInUser) {
			
			alertEntity.setMessage(alertDto.getMessage());
	     	AlertEntity updatedAlert = alertRepository.save(alertEntity);
			ModelMapper modelMapper = new ModelMapper();
			updatedAlertDto = modelMapper.map(updatedAlert, AlertDto.class);

			return updatedAlertDto;
		}
		
		throw new CenterUserServiceException(ErrorMessages.AUTHENTICATION_FAILED.getErrorMessage());
	}

	@Override
	public void deleteAlert(String alertId) throws Exception {

		AlertEntity alertEntity = alertRepository.findByAlertId(alertId);

		if (alertEntity == null) {

			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}

		alertRepository.delete(alertEntity);
	}

	@Override
	public List<AlertDto> getAlertsByUserId(String userId, int numberOfPage, int maxPages) {

		List<AlertDto> alertDtos = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();	
		CenterUserEntity centerUserEntity = centerUserRepository.findByUserId(userId);
		
		if(centerUserEntity == null) {
			
			return alertDtos;
		}
		
		if(numberOfPage > 0) {
			
			numberOfPage = numberOfPage - 1;
		}
		
		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);
		Page<AlertEntity> alertsPage = alertRepository.findAlertsByCenterUser(centerUserEntity, pageableRequest);
		List<AlertEntity> alerts = alertsPage.getContent();
		
		for(AlertEntity alertEntity : alerts) {
			
			AlertDto alertDto = new AlertDto();
			alertDto = modelMapper.map(alertEntity, AlertDto.class);
			alertDtos.add(alertDto);
		}
		
		return alertDtos;
	}

	@Override
	public List<AlertDto> getAlertsByCenter(String centerId) {

		List<AlertDto> alertDto = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();

		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {

			return alertDto;
		}

		Iterable<AlertEntity> alerts = alertRepository.findByCenterUser(centerEntity.getCenterUser());

		for (AlertEntity alertEntity : alerts) {

			alertDto.add(modelMapper.map(alertEntity, AlertDto.class));
		}

		return alertDto;
	}

	@Override
	public List<AlertDto> getAlerts(int numberOfPage, int maxPages) {

		List<AlertDto> listOfAlertDto = new ArrayList<>();

		if (numberOfPage > 0) {

			numberOfPage = numberOfPage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);
		Page<AlertEntity> alertsPage = alertRepository.findAll(pageableRequest);
		List<AlertEntity> alerts = alertsPage.getContent();

		for (AlertEntity alertEntity : alerts) {

			AlertDto alertDto = new AlertDto();
			ModelMapper modelMapper = new ModelMapper();
			alertDto = modelMapper.map(alertEntity, AlertDto.class);
			// BeanUtils.copyProperties(centerUserEntity, centerUserDto);

			listOfAlertDto.add(alertDto);
		}

		return listOfAlertDto;
	}

	@Override
	public List<AlertDto> getAlertsByPreference(int numberOfPage, int maxPages, List<String> groups, List<String> centers) {
		
		List<AlertDto> listOfAlertDto = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);
		Page<AlertEntity> alertsPage = alertRepository.findAllByFilter(groups, centers, pageableRequest);
		List<AlertEntity> alerts = alertsPage.getContent();

		for (AlertEntity alertEntity : alerts) {
			AlertDto alertDto = new AlertDto();
			ModelMapper modelMapper = new ModelMapper();
			alertDto = modelMapper.map(alertEntity, AlertDto.class);
			listOfAlertDto.add(alertDto);
		}

		return listOfAlertDto;
	}

	@Override
	public List<AlertDto> getAlertsSortedByDate(int numberOfPage, int maxPages) {
	
		List<AlertDto> listOfAlertDto = new ArrayList<>();
		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);
		Page<AlertEntity> alertsPage = alertRepository.findAllSortedByDate(pageableRequest);
		List<AlertEntity> alerts = alertsPage.getContent();

		for (AlertEntity alertEntity : alerts) {
			AlertDto alertDto = new AlertDto();
			ModelMapper modelMapper = new ModelMapper();
			alertDto = modelMapper.map(alertEntity, AlertDto.class);
			listOfAlertDto.add(alertDto);
		}

		return listOfAlertDto;
	}
}
