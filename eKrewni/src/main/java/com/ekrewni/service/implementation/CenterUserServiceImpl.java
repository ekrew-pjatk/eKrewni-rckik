package com.ekrewni.service.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.convention.NamingConventions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.entity.RoleEntity;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.io.repository.RoleRepository;
import com.ekrewni.security.UserPrincipal;
import com.ekrewni.service.CenterUserService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.CenterUserDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class CenterUserServiceImpl implements CenterUserService {

	@Autowired
	CenterUserRepository centerUserRepository;
	
	@Autowired
	CenterRepository centerRepository;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;


	@Autowired
	RoleRepository roleRepository;

	@Override
	public CenterUserDto createCenterUser(CenterUserDto centerUserDto) {

		CenterUserEntity storedCenterUserLogin = centerUserRepository.findByLogin(centerUserDto.getLogin());

		if (storedCenterUserLogin != null) {

			throw new CenterUserServiceException("Record already exist");

		}

		ModelMapper modelMapper = new ModelMapper();
		CenterUserEntity centerUserEntity = modelMapper.map(centerUserDto, CenterUserEntity.class);

		String publicCenterUserId = randomStringGenerator.generateCenterUserId(30);
		centerUserEntity.setUserId(publicCenterUserId);
		centerUserEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(centerUserDto.getPassword()));
		centerUserEntity.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));

		// Set roles
		Collection<RoleEntity> roleEntities = new HashSet<>();

		for (String role : centerUserDto.getRoles()) {

			RoleEntity roleEntity = roleRepository.findByName(role);

			if (roleEntity != null) {

				roleEntities.add(roleEntity);
			}
		}

		centerUserEntity.setRoles(roleEntities);
		
		CenterUserEntity storedCenterUserDetails = centerUserRepository.save(centerUserEntity);

		CenterUserDto createdCenterUserDto = modelMapper.map(storedCenterUserDetails, CenterUserDto.class);

		return createdCenterUserDto;

	}

	/**
	 * method to load user details from our database by login
	 */
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(login);

		if (centerUserEntity == null) {

			throw new UsernameNotFoundException(login);
		}

		return new UserPrincipal(centerUserEntity);
	}

	@Override
	public CenterUserDto getCenterUser(String login) {

		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(login);

		if (centerUserEntity == null) {

			throw new UsernameNotFoundException("CenterUser with login: " + login + " not found");
		}

		CenterUserDto centerUserDto = new CenterUserDto();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setFieldMatchingEnabled(true);
		modelMapper.getConfiguration().setFieldAccessLevel(AccessLevel.PROTECTED);
		modelMapper.getConfiguration().setSourceNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
		centerUserDto = modelMapper.map(centerUserEntity, CenterUserDto.class);

		return centerUserDto;
	}

	@Override
	public CenterUserDto getCenterUserByUserId(String centerUserId) {

		CenterUserDto centerUserDto = new CenterUserDto();
		CenterUserEntity centerUserEntity = centerUserRepository.findByUserId(centerUserId);

		if (centerUserEntity == null) {

			throw new UsernameNotFoundException("CenterUser with ID:" + centerUserId + " not found.");
		}

		ModelMapper modelMapper = new ModelMapper();
		centerUserDto = modelMapper.map(centerUserEntity, CenterUserDto.class);

		return centerUserDto;
	}
	
	@Override
	public CenterUserDto getCenterUserByCenterId(String centerId) {
		
		CenterUserDto centerUserDto = new CenterUserDto();
		
		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);
		CenterUserEntity centerUserEntity = centerUserRepository.findByCenter(centerEntity);
		
		if (centerUserEntity == null) {

			throw new UsernameNotFoundException("CenterUser with ID:" + centerId + " not found.");
		}
		
		ModelMapper modelMapper = new ModelMapper();
		centerUserDto = modelMapper.map(centerUserEntity, CenterUserDto.class);

		return centerUserDto;
	}

	@Override
	public CenterUserDto updateCenterUser(String centerUserId, CenterUserDto centerUserDto) {

		CenterUserDto updatedCenterUserDto = new CenterUserDto();
		CenterUserEntity centerUserEntity = centerUserRepository.findByUserId(centerUserId);

		if (centerUserEntity == null) {

			throw new CenterUserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}
        
		String password = centerUserDto.getPassword();
		String encryptedPassword = bCryptPasswordEncoder.encode(password);
		centerUserEntity.setEncryptedPassword(encryptedPassword);
		
		CenterUserEntity updatedCenterUserDetails = centerUserRepository.save(centerUserEntity);

		ModelMapper modelMapper = new ModelMapper();
		updatedCenterUserDto = modelMapper.map(updatedCenterUserDetails, CenterUserDto.class);

		return updatedCenterUserDto;
	}

	@Transactional
	@Override
	public void deleteCenterUser(String centerUserId) throws Exception {

		CenterUserEntity centerUserEntity = centerUserRepository.findByUserId(centerUserId);

		if (centerUserEntity == null) {

			throw new CenterUserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}
		centerUserRepository.delete(centerUserEntity);
	}

	@Override
	public List<CenterUserDto> getCenterUsers(int numberOfpage, int limitOfPages) {

		List<CenterUserDto> listOfCenterUserDto = new ArrayList<>();

		if (numberOfpage > 0) {

			numberOfpage = numberOfpage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfpage, limitOfPages);

		Page<CenterUserEntity> centerUsersPage = centerUserRepository.findAll(pageableRequest);

		List<CenterUserEntity> centerUsers = centerUsersPage.getContent();

		for (CenterUserEntity centerUserEntity : centerUsers) {

			CenterUserDto centerUserDto = new CenterUserDto();
			ModelMapper modelMapper = new ModelMapper();
			centerUserDto = modelMapper.map(centerUserEntity, CenterUserDto.class);

			listOfCenterUserDto.add(centerUserDto);
		}

		return listOfCenterUserDto;
	}

}
