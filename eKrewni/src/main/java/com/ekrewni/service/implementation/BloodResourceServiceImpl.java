package com.ekrewni.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.io.entity.BloodResourceEntity;
import com.ekrewni.io.entity.CenterEntity;
import com.ekrewni.io.entity.CenterUserEntity;
import com.ekrewni.io.repository.BloodResourceRepository;
import com.ekrewni.io.repository.CenterRepository;
import com.ekrewni.io.repository.CenterUserRepository;
import com.ekrewni.service.BloodResourceService;
import com.ekrewni.shared.RandomStringGenerator;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.ui.model.response.ErrorMessages;

@Service
public class BloodResourceServiceImpl implements BloodResourceService {

	@Autowired
	BloodResourceRepository bloodResourceRepository;

	@Autowired
	CenterRepository centerRepository;
	
	@Autowired
	CenterUserRepository centerUserRepository;

	@Autowired
	RandomStringGenerator randomStringGenerator;

	@Override
	public BloodResourceDto createBloodResource(BloodResourceDto bloodResourceDto) {

		if (bloodResourceRepository.findByBloodResourceId(bloodResourceDto.getBloodResourceId()) != null) {

			throw new RuntimeException("Record already exists");
		}

		ModelMapper modelMapper = new ModelMapper();

		BloodResourceEntity bloodResourceEntity = modelMapper.map(bloodResourceDto, BloodResourceEntity.class);
		String publicBloodResourceId = randomStringGenerator.generateBloodResourceId(30);
		bloodResourceEntity.setBloodResourceId(publicBloodResourceId);

		BloodResourceEntity storedBloodResource = bloodResourceRepository.save(bloodResourceEntity);
		BloodResourceDto createdBloodResourceDto = modelMapper.map(storedBloodResource, BloodResourceDto.class);

		return createdBloodResourceDto;
	}

	@Override
	public BloodResourceDto getBloodResourceByBloodResourceId(String bloodResourceId) {

		BloodResourceDto bloodResourceDto = new BloodResourceDto();
		BloodResourceEntity bloodResourceEntity = bloodResourceRepository.findByBloodResourceId(bloodResourceId);

		if (bloodResourceEntity == null) {

			throw new UsernameNotFoundException("BloodResource with ID: " + bloodResourceId + " not found");
		}

		ModelMapper modelMapper = new ModelMapper();
		bloodResourceDto = modelMapper.map(bloodResourceEntity, BloodResourceDto.class);

		return bloodResourceDto;
	}

	@Override
	public BloodResourceDto updateBloodResource(String bloodResourceId, BloodResourceDto bloodResourceDto)
			throws NameNotFoundException {

		BloodResourceDto updatedBloodResourceDto = new BloodResourceDto();
		BloodResourceEntity bloodResourceEntity = bloodResourceRepository.findByBloodResourceId(bloodResourceId);//

		if (bloodResourceEntity == null) {

			throw new UsernameNotFoundException("BloodResource with ID: " + bloodResourceId + " not found");
		}

		String centerIdOfResource = bloodResourceEntity.getCenter().getCenterId();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		CenterUserEntity centerUserEntity = centerUserRepository.findByLogin(currentPrincipalName);
		String centerIdOfLoggedInUser = centerUserEntity.getCenter().getCenterId();
		
		if(centerIdOfResource == centerIdOfLoggedInUser) {
			
			bloodResourceEntity.setVolume(bloodResourceDto.getVolume());
			BloodResourceEntity updatedBloodResource = bloodResourceRepository.save(bloodResourceEntity);
			ModelMapper modelMapper = new ModelMapper();
			updatedBloodResourceDto = modelMapper.map(updatedBloodResource, BloodResourceDto.class);
			
			return updatedBloodResourceDto;
		}

		throw new CenterUserServiceException(ErrorMessages.AUTHENTICATION_FAILED.getErrorMessage());
	}

	@Override
	public void deleteBloodResource(String bloodResourceId) throws Exception {

		BloodResourceEntity bloodResourceEntity = bloodResourceRepository.findByBloodResourceId(bloodResourceId);

		if (bloodResourceEntity == null) {

			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		}

		bloodResourceRepository.delete(bloodResourceEntity);
	}

	@Override
	public List<BloodResourceDto> getBloodResourceForGivenCenter(String centerId) {

		List<BloodResourceDto> bloodResourcesDto = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();

		CenterEntity centerEntity = centerRepository.findByCenterId(centerId);

		if (centerEntity == null) {

			return bloodResourcesDto;
		}

		Iterable<BloodResourceEntity> bloodResources = bloodResourceRepository.findAllByCenter(centerEntity);

		for (BloodResourceEntity bloodResourceEntity : bloodResources) {

			bloodResourcesDto.add(modelMapper.map(bloodResourceEntity, BloodResourceDto.class));
		}

		return bloodResourcesDto;
	}

	@Override
	public List<BloodResourceDto> getBloodResources(int numberOfPage, int maxPages) {

		List<BloodResourceDto> bloodResourceDtos = new ArrayList<>();

		if (numberOfPage > 0) {

			numberOfPage = numberOfPage - 1;
		}

		Pageable pageableRequest = PageRequest.of(numberOfPage, maxPages);

		Page<BloodResourceEntity> bloodResourcePage = bloodResourceRepository.findAll(pageableRequest);
		List<BloodResourceEntity> bloodResourceEntities = bloodResourcePage.getContent();

		for (BloodResourceEntity bloodResourceEntity : bloodResourceEntities) {

			BloodResourceDto bloodResourceDto = new BloodResourceDto();
			ModelMapper modelMapper = new ModelMapper();
			bloodResourceDto = modelMapper.map(bloodResourceEntity, BloodResourceDto.class);
			bloodResourceDtos.add(bloodResourceDto);
		}

		return bloodResourceDtos;
	}

}
