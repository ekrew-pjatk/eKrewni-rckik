package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.CenterMessageDto;

public interface CenterMessageService {

	CenterMessageDto createMessage(CenterMessageDto messageDto);

	CenterMessageDto getCenterMessageByMessageId(String messageId);

	CenterMessageDto updateCenterMessage(String messageId, CenterMessageDto messageDto) throws NameNotFoundException;

	void deleteCenterMessage(String messageId) throws Exception;
	
	List<CenterMessageDto> getCentersMessages(int page, int limit);

}
