package com.ekrewni.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ekrewni.shared.dto.CenterUserDto;

public interface CenterUserService extends UserDetailsService {

	CenterUserDto createCenterUser(CenterUserDto userDto);

	CenterUserDto getCenterUser(String login);

	CenterUserDto getCenterUserByUserId(String centerUserId);

	CenterUserDto getCenterUserByCenterId(String centerId);

	CenterUserDto updateCenterUser(String centerUserId, CenterUserDto centerUserDto);

	void deleteCenterUser(String centerUserId) throws Exception;

	List<CenterUserDto> getCenterUsers(int page, int limit);

}
