package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.BloodResourceDto;

public interface BloodResourceService {

	BloodResourceDto createBloodResource(BloodResourceDto bloodResourceDto);

	BloodResourceDto getBloodResourceByBloodResourceId(String bloodResourceId);

	BloodResourceDto updateBloodResource(String bloodResourceId, BloodResourceDto bloodResourceDto) throws NameNotFoundException;

	void deleteBloodResource(String bloodResourceId) throws Exception;

	List<BloodResourceDto> getBloodResourceForGivenCenter(String bloodResourceId);

	List<BloodResourceDto> getBloodResources(int numberOfPage, int maxPages);

}
