package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.AlertDto;

public interface AlertService {

	AlertDto createAlert(AlertDto alert);

	AlertDto getAlertByAlertId(String alertId);

	AlertDto updateAlert(String alertId, AlertDto alertDto) throws NameNotFoundException;

	List<AlertDto> getAlertsByUserId(String userId, int numberOfPage, int maxPages);

	void deleteAlert(String alertId) throws Exception;

	List<AlertDto> getAlerts(int numberOfPage, int maxPages);

	List<AlertDto> getAlertsByCenter(String centerId);

	List<AlertDto> getAlertsSortedByDate(int numberOfPage, int maxPages);

	List<AlertDto> getAlertsByPreference(int numberOfPage, int maxPages, List<String> bloodGroups, List<String> centers);
}
