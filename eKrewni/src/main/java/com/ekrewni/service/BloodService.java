package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.BloodDto;

public interface BloodService {

	BloodDto createBloodGroup(BloodDto blood);

	BloodDto getBloodByBloodType(String bloodType);

	BloodDto getBloodByRh(String rh);

	BloodDto getBloodByBloodId(String bloodId);

	BloodDto updateBlood(String bloodId, BloodDto bloodDto) throws NameNotFoundException;

	void deleteBlood(String bloodId) throws Exception;

	List<BloodDto> getBloodGroups(int numberOfPage, int limitOfPages);
	
}
