package com.ekrewni.service;

import java.util.List;

import javax.naming.NameNotFoundException;

import com.ekrewni.shared.dto.CenterDto;

public interface CenterService {

	CenterDto createCenter(CenterDto center);

	CenterDto getCenterByCenterId(String centerId);

	CenterDto updateCenter(String centerId, CenterDto center) throws NameNotFoundException;

	void deleteCenter(String centerId) throws Exception;

	List<CenterDto> getCenters(int numberOfPage, int maxPages);

}
