package com.ekrewni.exceptions;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.ekrewni.ui.model.response.ErrorMessage;

@ControllerAdvice
public class ExceptionAppHandler {

	@ExceptionHandler(value = { CenterUserServiceException.class, NullPointerException.class })
	public ResponseEntity<Object> handleCenterUserServiceException(
			CenterUserServiceException centerUserServiceException, WebRequest webRequest) {

		ErrorMessage errorMessage = new ErrorMessage(new Date(), centerUserServiceException.getMessage());

		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleOtherException(Exception exception, WebRequest webRequest) {

		ErrorMessage errorMessage = new ErrorMessage(new Date(), exception.getMessage());

		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}

}
