package com.ekrewni.exceptions;

public class CenterUserServiceException extends RuntimeException {

	private static final long serialVersionUID = 1348771109171435607L;

	public CenterUserServiceException(String exceptionMessage) {

		super(exceptionMessage);
	}

}
