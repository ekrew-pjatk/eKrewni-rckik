package com.ekrewni.shared.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AlertDto implements Serializable {

	private static final long serialVersionUID = -8127285146192953389L;
	private long id;
	private String alertId;
	private String message;
	private LocalDateTime date;
	private String centerUserLogin;
	private BloodDto blood;
	private CenterUserDto centerUser;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getAlertId() {

		return alertId;
	}

	public void setAlertId(String alertId) {

		this.alertId = alertId;
	}

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = message;
	}

	public LocalDateTime getDate() {

		return date;
	}

	public void setDate(LocalDateTime date) {

		this.date = date;
	}

	public BloodDto getBlood() {

		return blood;
	}

	public void setBlood(BloodDto blood) {

		this.blood = blood;
	}

	public String pobierzCenterUserLogin() {

		return centerUserLogin;
	}

	public void ustawCenterUserLogin(String centerUserLogin) {

		this.centerUserLogin = centerUserLogin;
	}

	public CenterUserDto getCenterUser() {

		return centerUser;
	}

	public void setCenterUser(CenterUserDto centerUser) {

		this.centerUser = centerUser;
	}
	
}
