package com.ekrewni.shared.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class CenterUserDto implements Serializable {

	private static final long serialVersionUID = 6696822356544789095L;
	private long id;
	private String userId;
	private String login;
	private String password;
	private String encryptedPassword;
	private CenterDto center;
	private List<CenterMessageDto> centerMessages;
	private List<AlertDto> alerts;
	private Collection<String> roles;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getUserId() {

		return userId;
	}

	public void setUserId(String userId) {

		this.userId = userId;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getEncryptedPassword() {

		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {

		this.encryptedPassword = encryptedPassword;
	}

	public String getLogin() {

		return login;
	}

	public void setLogin(String login) {

		this.login = login;
	}

	public CenterDto getCenter() {

		return center;
	}

	public void setCenter(CenterDto center) {

		this.center = center;
	}

	public List<CenterMessageDto> getCenterMessages() {

		return centerMessages;
	}

	public void setCenterMessages(List<CenterMessageDto> centerMessages) {

		this.centerMessages = centerMessages;
	}

	public List<AlertDto> getAlerts() {

		return alerts;
	}

	public void setAlerts(List<AlertDto> alerts) {

		this.alerts = alerts;
	}

	public Collection<String> getRoles() {

		return roles;
	}

	public void setRoles(Collection<String> roles) {

		this.roles = roles;
	}

}
