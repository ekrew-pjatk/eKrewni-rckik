package com.ekrewni.shared.dto;

public class BloodResourceDto {

	private long id;
	private String bloodResourceId;
	private double volume;
	private CenterDto center;
	private BloodDto blood;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getBloodResourceId() {

		return bloodResourceId;
	}

	public void setBloodResourceId(String bloodResourceId) {

		this.bloodResourceId = bloodResourceId;
	}

	public double getVolume() {

		return volume;
	}

	public void setVolume(double volume) {

		this.volume = volume;
	}

	public CenterDto getCenter() {

		return center;
	}

	public void setCenter(CenterDto center) {

		this.center = center;
	}

	public BloodDto getBlood() {

		return blood;
	}

	public void setBlood(BloodDto blood) {

		this.blood = blood;
	}
}
