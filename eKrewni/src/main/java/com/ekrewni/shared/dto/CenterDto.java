package com.ekrewni.shared.dto;

import java.io.Serializable;
import java.util.List;

public class CenterDto implements Serializable {

	private static final long serialVersionUID = 8859887363571602108L;
	private long id;
	private String centerId;
	private String name;
	private String phoneNumber;
	private String email;
	private String webSite;
	private AddressDto address;
	private List<BloodResourceDto> bloodResources;
	private CenterUserDto centerUser;
	
	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getCenterId() {

		return centerId;
	}

	public void setCenterId(String centerId) {

		this.centerId = centerId;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getPhoneNumber() {

		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getWebSite() {

		return webSite;
	}

	public void setWebSite(String webSite) {

		this.webSite = webSite;
	}

	public AddressDto getAddress() {

		return address;
	}

	public void setAddress(AddressDto address) {

		this.address = address;
	}

	public List<BloodResourceDto> getBloodResources() {

		return bloodResources;
	}

	public void setBloodResources(List<BloodResourceDto> bloodResources) {

		this.bloodResources = bloodResources;
	}

	public CenterUserDto getCenterUser() {

		return centerUser;
	}

	public void setCenterUser(CenterUserDto centerUser) {

		this.centerUser = centerUser;
	}
	
}
