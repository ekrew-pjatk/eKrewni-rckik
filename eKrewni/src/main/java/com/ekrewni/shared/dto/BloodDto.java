package com.ekrewni.shared.dto;

import java.io.Serializable;
import java.util.List;

public class BloodDto implements Serializable {

	private static final long serialVersionUID = 5329054444299696305L;
	private long id;
	private String bloodId;
	private String bloodType;
	private String rh;
	private List<BloodResourceDto> bloodResourceDtos;
	private List<AlertDto> alerts;
	
	public List<BloodResourceDto> getBloodResourceDtos() {
	
		return bloodResourceDtos;
		
	}
	
	public void setBloodResourceDtos(List<BloodResourceDto> bloodResourceDtos) {
	
		this.bloodResourceDtos = bloodResourceDtos;
		
	}

	public long getId() {

		return id;
		
	}

	public void setId(long id) {

		this.id = id;
		
	}

	public String getBloodType() {

		return bloodType;
		
	}

	public void setBloodType(String bloodType) {

		this.bloodType = bloodType;
		
	}

	public String getRh() {

		return rh;
		
	}

	public void setRh(String rh) {

		this.rh = rh;
		
	}

	public String getBloodId() {

		return bloodId;

	}

	public void setBloodId(String bloodId) {

		this.bloodId = bloodId;

	}

	public List<AlertDto> getAlerts() {

		return alerts;
		
	}

	public void setAlerts(List<AlertDto> alerts) {

		this.alerts = alerts;
		
	}
	
}
