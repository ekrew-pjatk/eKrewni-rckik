package com.ekrewni.shared.dto;

import java.io.Serializable;

public class AddressDto implements Serializable {

	private static final long serialVersionUID = -3949734979790871736L;
	private long id;
	private String addressId;
	private String city;
	private String postalCode;
	private String street;
    private String latitude;
	private String longitude;
	private CenterDto center;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getAddressId() {

		return addressId;
	}

	public void setAddressId(String addressId) {

		this.addressId = addressId;
	}

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getPostalCode() {

		return postalCode;
	}

	public void setPostalCode(String postalCode) {

		this.postalCode = postalCode;
	}

	public String getStreet() {

		return street;
	}

	public void setStreet(String street) {

		this.street = street;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public CenterDto getCenter() {

		return center;
	}

	public void setCenter(CenterDto center) {

		this.center = center;
	}

}
