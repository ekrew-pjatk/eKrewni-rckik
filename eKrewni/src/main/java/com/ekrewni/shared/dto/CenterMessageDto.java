package com.ekrewni.shared.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CenterMessageDto implements Serializable {

	private static final long serialVersionUID = -7218726629079175065L;
	private long id;
	private String centerMessageId;
	private String text;
	private LocalDateTime dateTime;
	private String centerUserLogin;
	private CenterUserDto centerUserDto;

	public long getId() {

		return id;
	}

	public void setId(long id) {

		this.id = id;
	}

	public String getCenterMessageId() {

		return centerMessageId;
	}

	public void setCenterMessageId(String centerMessageId) {

		this.centerMessageId = centerMessageId;
	}

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}

	public LocalDateTime getDateTime() {

		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {

		this.dateTime = dateTime;
	}

	public String wezCenterUserLogin() {
		
		return centerUserLogin;
	}

	public void umiescCenterUserLogin(String centerUserLogin) {
		
		this.centerUserLogin = centerUserLogin;
	}

	public CenterUserDto getCenterUserDto() {
		
		return centerUserDto;
	}

	public void setCenterUserDto(CenterUserDto centerUserDto) {
		
		this.centerUserDto = centerUserDto;
	}
	
}
