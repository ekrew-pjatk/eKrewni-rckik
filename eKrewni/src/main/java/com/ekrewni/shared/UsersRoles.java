package com.ekrewni.shared;

public enum UsersRoles {
	ROLE_USER, ROLE_MED_ADMIN, ROLE_TECH_ADMIN
}
