package com.ekrewni.shared;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomStringGenerator {

	private final Random RANDOM = new SecureRandom();
	private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public String generateCenterUserId(int length) {

		return generateRandomString(length);
	}

	public String generateAddressId(int length) {

		return generateRandomString(length);
	}

	public String generateAlertId(int length) {

		return generateRandomString(length);
	}

	public String generateBloodId(int length) {

		return generateRandomString(length);
	}

	public String generateCenterId(int length) {

		return generateRandomString(length);
	}

	public String generateCenterMessageId(int length) {

		return generateRandomString(length);
	}

	public String generateBloodResourceId(int length) {

		return generateRandomString(length);
	}

	public String generateInitialPasswordForCenterUsers(int length) {

		return generateRandomString(length);
	}

	private String generateRandomString(int length) {

		StringBuilder randomString = new StringBuilder(length);

		for (int i = 0; i < length; i++) {

			randomString.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));

		}

		return new String(randomString);
	}

}
