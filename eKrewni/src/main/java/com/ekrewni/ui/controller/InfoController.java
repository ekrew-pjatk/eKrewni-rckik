package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.ui.model.response.InfoForDonorRest;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("info") // http://localhost:8080/ekrewni/info
public class InfoController {

	@ApiOperation(value = "Get contraindications to be a blood donor", notes = "${infoController.getContraindicationsForDonors.apiDesc}")
	@GetMapping(path = "/contraindinations", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public List<InfoForDonorRest> getContraindicationsForDonors() {

		List<InfoForDonorRest> listOfContraindicationForDonorsRest = new ArrayList<>();

		InfoForDonorRest age = new InfoForDonorRest();
		age.setInfo("Wiek: 18-65 lat");

		InfoForDonorRest moreThan60Years = new InfoForDonorRest();
		moreThan60Years.setInfo("Wiek powyżej 60 lat: po decyzji lekarza w jednostce organizacyjnej publicznej służby krwi");

		InfoForDonorRest moreThan65Years = new InfoForDonorRest();
		moreThan65Years.setInfo(
				"Wiek powyżej 65 lat: po corocznym uzyskaniu zgody lekarza w jednostce organizacyjnej publicznej służby krwi");

		InfoForDonorRest bodyMass = new InfoForDonorRest();
		bodyMass.setInfo("Masa ciała: większa lub równa 50 kg");

		InfoForDonorRest bodyMassForMoreThanOneBlood = new InfoForDonorRest();
		bodyMassForMoreThanOneBlood.setInfo(
				"Masa ciała dla dawców oddających 2 jednostki koncentratu krwinek czerwonych: większa lub równa 70 kg");

		InfoForDonorRest bloodPressureDown = new InfoForDonorRest();
		bloodPressureDown.setInfo("Ciśnienie rozkurczowe: do 100 mm Hg");

		InfoForDonorRest bloodPressureUp = new InfoForDonorRest();
		bloodPressureUp.setInfo("Ciśnienie skurczowe: do 180 mm Hg");

		InfoForDonorRest pulse = new InfoForDonorRest();
		pulse.setInfo(
				"Tętno: miarowe, o częstości od 50 do 100 uderzeń na minutę (niższe wartości tętna dopuszczane są u osób wysportowanych, z dobrą tolerancją wysiłku)");

		listOfContraindicationForDonorsRest.add(age);
		listOfContraindicationForDonorsRest.add(moreThan60Years);
		listOfContraindicationForDonorsRest.add(moreThan65Years);
		listOfContraindicationForDonorsRest.add(bodyMass);
		listOfContraindicationForDonorsRest.add(bodyMassForMoreThanOneBlood);
		listOfContraindicationForDonorsRest.add(bloodPressureDown);
		listOfContraindicationForDonorsRest.add(bloodPressureUp);
		listOfContraindicationForDonorsRest.add(pulse);

		return listOfContraindicationForDonorsRest;
	}
	
	@ApiOperation(value = "Get circumstances for being a blood donor", notes = "${infoController.getCircumstancesForBeingDonor.apiDesc}")
	@GetMapping(path = "/circumstances", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public List<InfoForDonorRest> getCircumstancesForBeingDonor() {
		
		List<InfoForDonorRest> listOfCircumstances = new ArrayList<>();
		
		InfoForDonorRest circulatorySystem = new InfoForDonorRest();
		circulatorySystem.setInfo("Choroby układu krążenia: aktywna lub przebyta poważna choroba układu krążenia, oprócz wad wrodzonych całkowicie wyleczonych");
		
		InfoForDonorRest nervousSystem = new InfoForDonorRest();
		nervousSystem.setInfo("Choroby układu nerwowego: poważna choroba aktywna, przewlekła lub nawracająca; przebyta poważna choroba ośrodkowego układu nerwowego");
		
		InfoForDonorRest pathologicalBleeding = new InfoForDonorRest();
		pathologicalBleeding.setInfo("Skłonność do patologicznych krwawień: zaburzenia krzepnięcia w wywiadzie");
		
		InfoForDonorRest deliquium = new InfoForDonorRest();
		deliquium.setInfo("Nawracające omdlenia albo napady drgawkowe: poza drgawkami wieku dziecięcego lub sytuacją, w której co najmniej przez 3 lata po zakończeniu leczenia nie obserwuje się nawracających drgawek");
		
		InfoForDonorRest digestiveSystem = new InfoForDonorRest();
		digestiveSystem.setInfo("Choroby układu pokarmowego: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest respiratorySystem = new InfoForDonorRest();
		respiratorySystem.setInfo("Choroby układu oddechowego: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest genitourinarySystem = new InfoForDonorRest();
		genitourinarySystem.setInfo("Choroby układu moczowo-płciowego i nerek: choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest immuneSystem = new InfoForDonorRest();
		immuneSystem.setInfo("Choroby układu immunologicznego: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest endocrynologySystem = new InfoForDonorRest();
		endocrynologySystem.setInfo("Choroby metaboliczne i choroby układu endokrynnego: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest bloodSystem = new InfoForDonorRest();
		bloodSystem.setInfo("Choroby krwi i układu krwiotwórczego: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest dermatology = new InfoForDonorRest();
		dermatology.setInfo("Choroby skóry: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest agreements = new InfoForDonorRest();
		agreements.setInfo("Choroby układowe: poważna choroba aktywna, przewlekła lub nawracająca");
		
		InfoForDonorRest diabetes = new InfoForDonorRest();
		diabetes.setInfo("Cukrzyca: kandydaci na dawców krwi lub dawcy krwi z rozpoznaną cukrzycą");
		
		InfoForDonorRest cancers = new InfoForDonorRest();
		cancers.setInfo("Choroby nowotworowe: nowotwór złośliwy");
		
		InfoForDonorRest zymosis = new InfoForDonorRest();
		zymosis.setInfo("Choroby zakaźne: zakażenie wirusem zapalenia wątroby typu B, zakażenie wirusem zapalenia wątroby typu C, wirusowe zapalenie wątroby o nieustalonym rodzaju wirusa w wywiadzie, żółtaczka o niejasnej etiologii, HIV, HTLV, babeszjoza, kala-azar (leiszmanioza trzewna), trypanosomoza amerykańska (gorączka Chagasa), promienica, tularemia, gorączka Q");
		
		InfoForDonorRest creutzfeld = new InfoForDonorRest();
		creutzfeld.setInfo("Gąbczaste zwyrodnienie mózgu (TSE) (np. choroba Creutzfeldta-Jakoba, wariant choroby Creutzfeldta-Jakoba)");
		
		InfoForDonorRest syphilis = new InfoForDonorRest();
		syphilis.setInfo("Kiła");
		
		InfoForDonorRest injection = new InfoForDonorRest();
		injection.setInfo("Produkty lecznicze stosowane drogą iniekcji: każdy przypadek stosowania produktów leczniczych w postaci zastrzyków, które nie zostały przepisane przez lekarza");
		
		InfoForDonorRest mentalDistress = new InfoForDonorRest();
		mentalDistress.setInfo("Zaburzenia psychiczne i zaburzenia zachowania spowodowane używaniem substancji psychoaktywnych");
		
		InfoForDonorRest transplant = new InfoForDonorRest();
		transplant.setInfo("Biorcy ksenoprzeszczepów");
		
		InfoForDonorRest anaphylacticReaction = new InfoForDonorRest();
		anaphylacticReaction.setInfo("Reakcja anafilaktyczna: każdy przypadek przebycia");
		
		listOfCircumstances.add(circulatorySystem);
		listOfCircumstances.add(nervousSystem);
		listOfCircumstances.add(pathologicalBleeding);
		listOfCircumstances.add(deliquium);
		listOfCircumstances.add(digestiveSystem);
		listOfCircumstances.add(respiratorySystem);
		listOfCircumstances.add(genitourinarySystem);
		listOfCircumstances.add(immuneSystem);
		listOfCircumstances.add(endocrynologySystem);
		listOfCircumstances.add(bloodSystem);
		listOfCircumstances.add(dermatology);
		listOfCircumstances.add(agreements);
		listOfCircumstances.add(diabetes);
		listOfCircumstances.add(cancers);
		listOfCircumstances.add(zymosis);
		listOfCircumstances.add(creutzfeld);
		listOfCircumstances.add(syphilis);
		listOfCircumstances.add(injection);
		listOfCircumstances.add(mentalDistress);
		listOfCircumstances.add(transplant);
		listOfCircumstances.add(anaphylacticReaction);
		
		
		return listOfCircumstances;
	}

}
