package com.ekrewni.ui.controller;

import com.ekrewni.service.AlertService;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.ui.model.response.SearchAlertRest;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("search")
public class SearchController {

	@Autowired
	AlertService alertService;

	@ApiOperation(value = "Get filtered list of Alerts endpoint", notes = "${searchController.getFilteredAlerts.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<SearchAlertRest> getFilteredAlerts(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "10") int maxPages,
			@RequestParam(value = "groups", required = false) List<String> groups,
			@RequestParam(value = "centers", required = false) List<String> centers) {

		List<SearchAlertRest> listOfAlertsRests = new ArrayList<>();
		List<AlertDto> listOfAlertsDto;

		if(groups == null && centers == null) {
			listOfAlertsDto = alertService.getAlertsSortedByDate(numberOfPage, maxPages);
		} else {
 			listOfAlertsDto = alertService.getAlertsByPreference(numberOfPage, maxPages, groups, centers);
		}


		for (AlertDto alertDto : listOfAlertsDto) {

			SearchAlertRest searchAlertModel = new SearchAlertRest();
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.addMappings(centerUserToCenterIdMapping);
			searchAlertModel = modelMapper.map(alertDto, SearchAlertRest.class);

			listOfAlertsRests.add(searchAlertModel);
		}

		return listOfAlertsRests;
	}

	@ApiOperation(value = "Get Alert endpoint", notes = "${searchController.getAlertWithCenterId.apiDesc}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public SearchAlertRest getAlertWithCenterId(@PathVariable String id) {

		SearchAlertRest alertResponse = new SearchAlertRest();

		AlertDto alertDto = alertService.getAlertByAlertId(id);
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.addMappings(centerUserToCenterIdMapping);

		alertResponse = modelMapper.map(alertDto, SearchAlertRest.class);

		return alertResponse;
	}

	PropertyMap<AlertDto, SearchAlertRest> centerUserToCenterIdMapping = new PropertyMap<AlertDto, SearchAlertRest>() {
		protected void configure() {
			map().setCenterId(source.getCenterUser().getCenter().getCenterId());
		}
	};
}
