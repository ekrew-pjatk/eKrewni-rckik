package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.service.BloodService;
import com.ekrewni.shared.dto.BloodDto;
import com.ekrewni.ui.model.request.BloodDetailsRequestModel;
import com.ekrewni.ui.model.response.BloodRest;
import com.ekrewni.ui.model.response.OperationStatusModel;
import com.ekrewni.ui.model.response.RequestOperationStatus;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("bloodgroups") // http://localhost:8080/ekrewni/bloodgroups
public class BloodController {

	@Autowired
	BloodService bloodService;

	@ApiOperation(value = "Get Blood endpoint", notes = "${bloodController.getBlood.apiDesc}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public BloodRest getBloodGroup(@PathVariable String id) {

		BloodRest bloodRest = new BloodRest();

		BloodDto bloodDto = bloodService.getBloodByBloodId(id);
		ModelMapper modelMapper = new ModelMapper();
		bloodRest = modelMapper.map(bloodDto, BloodRest.class);

		return bloodRest;
	}

	@ApiOperation(value = "Get All Blood endpoint", notes = "${bloodController.getAllBlood.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<BloodRest> getAllBloodGroups(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<BloodRest> listOfBloods = new ArrayList<>();

		List<BloodDto> listOfBloodsDto = bloodService.getBloodGroups(numberOfPage, maxPages);

		for (BloodDto bloodDto : listOfBloodsDto) {

			BloodRest bloodModel = new BloodRest();
			ModelMapper modelMapper = new ModelMapper();
			bloodModel = modelMapper.map(bloodDto, BloodRest.class);
			listOfBloods.add(bloodModel);
		}

		return listOfBloods;
	}

	@ApiOperation(value = "Create Blood endpoint", notes = "${bloodController.createBlood.apiDesc}")
	@PostMapping
	public BloodRest createBloodGroup(@RequestBody BloodDetailsRequestModel bloodDetails) {

		BloodRest bloodRest = new BloodRest();

		if (bloodDetails.getBloodType().isEmpty()) {

			throw new NullPointerException("The object is null");
		}

		ModelMapper modelMapper = new ModelMapper();
		BloodDto bloodDto = modelMapper.map(bloodDetails, BloodDto.class);

		BloodDto createdBlood = bloodService.createBloodGroup(bloodDto);
		bloodRest = modelMapper.map(createdBlood, BloodRest.class);

		return bloodRest;
	}

	@ApiOperation(value = "Update Blood endpoint", notes = "${bloodController.updateBlood.apiDesc}")
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public BloodRest updateBloodGroup(@PathVariable String id, @RequestBody BloodDetailsRequestModel bloodRequestModel)
			throws NameNotFoundException {

		BloodRest bloodRest = new BloodRest();
		ModelMapper modelMapper = new ModelMapper();
		BloodDto bloodDto = modelMapper.map(bloodRequestModel, BloodDto.class);

		BloodDto updateBlood = bloodService.updateBlood(id, bloodDto);
		bloodRest = modelMapper.map(updateBlood, BloodRest.class);

		return bloodRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Delete Blood endpoint", notes = "${bloodController.deleteBlood.apiDesc}")
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel deleteBlood(@PathVariable String id) throws Exception {

		OperationStatusModel deletedBlood = new OperationStatusModel();
		deletedBlood.setNameOfOperation(RequestOperationName.DELETE.name());

		bloodService.deleteBlood(id);

		deletedBlood.setResult(RequestOperationStatus.SUCCESS.name());
		return deletedBlood;
	}

}
