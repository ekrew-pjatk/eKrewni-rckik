package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.security.access.prepost.PostAuthorize;
//import org.springframework.security.access.prepost.PreAuthorize;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.service.AlertService;
import com.ekrewni.service.CenterService;
import com.ekrewni.service.CenterUserService;
import com.ekrewni.shared.UsersRoles;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.shared.dto.CenterDto;
import com.ekrewni.shared.dto.CenterUserDto;
import com.ekrewni.ui.model.request.CenterUserDetailsRequestModel;
import com.ekrewni.ui.model.request.CenterUserWithCenterIdRequestModel;
import com.ekrewni.ui.model.response.AlertRest;
import com.ekrewni.ui.model.response.CenterRest;
import com.ekrewni.ui.model.response.CenterUserRest;
import com.ekrewni.ui.model.response.ErrorMessages;
import com.ekrewni.ui.model.response.OperationStatusModel;
import com.ekrewni.ui.model.response.RequestOperationStatus;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("centerusers") // http://localhost:8080/ekrewni/centerusers
public class CenterUserController {

	@Autowired
	CenterUserService userService;

	@Autowired
	AlertService alertService;

	@Autowired
	CenterService centerService;

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Get CenterUser endpoint", notes = "${centerUserController.getCenterUser.apiDesc}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public CenterUserRest getCenterUser(@PathVariable String id) {

		CenterUserRest centerUserRest = new CenterUserRest();

		CenterUserDto centerUserDto = userService.getCenterUserByUserId(id);
		ModelMapper modelMapper = new ModelMapper();
		centerUserRest = modelMapper.map(centerUserDto, CenterUserRest.class);

		return centerUserRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Get center of CenterUsers endpoint", notes = "${centerUserController.getCenterByCenterUser.apiDesc}")
	@GetMapping(path = "/{userId}/center", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public CenterRest getCentersUser(@PathVariable String userId) {

		CenterUserDto centerUserDto = userService.getCenterUserByUserId(userId);
		CenterDto centerDto = new CenterDto();
		String centerId = centerUserDto.getCenter().getCenterId();
		centerDto = centerService.getCenterByCenterId(centerId);
		centerDto.setCenterUser(centerUserDto);
		CenterRest centerModel = new CenterRest();
		ModelMapper modelMapper = new ModelMapper();
		centerModel = modelMapper.map(centerDto, CenterRest.class);
		return centerModel;

	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Create CenterUser endpoint", notes = "${centerUserController.createCenterUser.apiDesc}")
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public CenterUserRest createCenterUser(
			@RequestBody CenterUserWithCenterIdRequestModel centerUserWithCenterRequestModel) throws Exception {

		CenterUserRest centerUserRest = new CenterUserRest();

		if (centerUserWithCenterRequestModel.getLogin().isEmpty()) {

			throw new CenterUserServiceException(ErrorMessages.REQUIRED_FIELD_IS_MISSING.getErrorMessage());
		}

		String addedUsersCenterId = centerUserWithCenterRequestModel.getCenterId();

		String login = centerUserWithCenterRequestModel.getLogin();
		String password = centerUserWithCenterRequestModel.getPassword();

		CenterUserDetailsRequestModel centerUserDetailsRequestModel = new CenterUserDetailsRequestModel();
		centerUserDetailsRequestModel.setLogin(login);
		centerUserDetailsRequestModel.setPassword(password);

		ModelMapper modelMapper = new ModelMapper();
		CenterUserDto userDto = modelMapper.map(centerUserDetailsRequestModel, CenterUserDto.class);

		userDto.setRoles(new HashSet<>(Arrays.asList(UsersRoles.ROLE_USER.name())));
		CenterDto centerFromCenterId = centerService.getCenterByCenterId(addedUsersCenterId);
		userDto.setCenter(centerFromCenterId);

		CenterUserDto createdUser = userService.createCenterUser(userDto);
		centerUserRest = modelMapper.map(createdUser, CenterUserRest.class);

		return centerUserRest;

	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Update CenterUser endpoint", notes = "${centerUserController.updateCenterUser.apiDesc}")
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel updateUser(@PathVariable String id,
			@RequestBody CenterUserDetailsRequestModel centerUserDetailsRequest) {

		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setNameOfOperation(RequestOperationName.PASSWORD_RESET.name());

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();

		CenterUserDto centerUserDto = new CenterUserDto();
		centerUserDto = userService.getCenterUserByUserId(id);

		String adminLogin = "techAdmin";
		String login = centerUserDto.getLogin();

		if (currentPrincipalName.equals(adminLogin) || (login.equals(currentPrincipalName))) {

			ModelMapper modelMapper = new ModelMapper();
			CenterUserDto centerUserDtoFromRequest = modelMapper.map(centerUserDetailsRequest, CenterUserDto.class);

			userService.updateCenterUser(id, centerUserDtoFromRequest);

			operationStatusModel.setResult(RequestOperationStatus.SUCCESS.name());
			return operationStatusModel;
		}

		throw new CenterUserServiceException(ErrorMessages.AUTHENTICATION_FAILED.getErrorMessage());
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Delete CenterUser endpoint", notes = "${centerUserController.deleteCenterUser.apiDesc}")
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteUser(@PathVariable String id) throws Exception {

		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setNameOfOperation(RequestOperationName.DELETE.name());

		userService.deleteCenterUser(id);

		operationStatusModel.setResult(RequestOperationStatus.SUCCESS.name());
		return operationStatusModel;

	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Get list of CenterUsers endpoint", notes = "${centerUserController.getAllCenterUser.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<CenterUserRest> getUsers(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<CenterUserRest> listOfCenterUserRests = new ArrayList<>();

		List<CenterUserDto> listOfCenterUsersDti = userService.getCenterUsers(numberOfPage, maxPages);

		for (CenterUserDto centerUserDto : listOfCenterUsersDti) {

			CenterUserRest centerUserModel = new CenterUserRest();
			ModelMapper modelMapper = new ModelMapper();
			centerUserModel = modelMapper.map(centerUserDto, CenterUserRest.class);
			listOfCenterUserRests.add(centerUserModel);
		}

		return listOfCenterUserRests;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Get list of Alerts by CenterUser endpoint", notes = "${centerUserController.getAllAlertsByCenterUser.apiDesc}")
	@GetMapping(path = "/{centerUserId}/alerts", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<AlertRest> getUserAlerts(@PathVariable String centerUserId,
			@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<AlertRest> listOfAlertRest = new ArrayList<>();
		List<AlertDto> listOfAlertDto = alertService.getAlertsByUserId(centerUserId, numberOfPage, maxPages);

		for (AlertDto alertDto : listOfAlertDto) {

			AlertRest alertModel = new AlertRest();
			ModelMapper modelMapper = new ModelMapper();
			alertModel = modelMapper.map(alertDto, AlertRest.class);
			listOfAlertRest.add(alertModel);
		}

		return listOfAlertRest;
	}

}
