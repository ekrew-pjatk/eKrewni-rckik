package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.service.AddressService;
import com.ekrewni.shared.dto.AddressDto;
import com.ekrewni.ui.model.response.CoordinatesRest;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("coordinates") // http://localhost:8080/ekrewni/coordinates
public class CoordinatesController {
	
	@Autowired
	AddressService addressService;
	
	
	@ApiOperation(value = "Get longitude and latitude coordinates for each city", notes = "${coordinatesController.getAllAddresses.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<CoordinatesRest> getAllLongLatCoordinates(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<CoordinatesRest> listOfCoordinates = new ArrayList<>();

		List<AddressDto> listOfAddressDto = addressService.getAddresses(numberOfPage, maxPages);

		for (AddressDto addressDto : listOfAddressDto) {

			CoordinatesRest coordinatesModel = new CoordinatesRest();
			ModelMapper modelMapper = new ModelMapper();
			coordinatesModel = modelMapper.map(addressDto, CoordinatesRest.class);
			listOfCoordinates.add(coordinatesModel);
		}

		return listOfCoordinates;
	}

}
