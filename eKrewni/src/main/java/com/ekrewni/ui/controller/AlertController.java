package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.service.AlertService;
import com.ekrewni.service.BloodService;
import com.ekrewni.service.CenterUserService;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.ui.model.request.AlertRequestModel;
import com.ekrewni.ui.model.response.AlertRest;
import com.ekrewni.ui.model.response.OperationStatusModel;
import com.ekrewni.ui.model.response.RequestOperationStatus;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("alerts") // http://localhost:8080/ekrewni/alerts
public class AlertController {

	@Autowired
	AlertService alertService;

	@Autowired
	CenterUserService centerUserService;

	@Autowired
	BloodService bloodService;

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Create Alert endpoint", notes = "${alertController.createAlert.apiDesc}")
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public AlertRest createAlert(@RequestBody AlertRequestModel alert) throws Exception {

		AlertRest alertRest = new AlertRest();

		if (alert.getMessage().isEmpty()) {

			throw new NullPointerException("The object is null");
		}

		/*
		 * POBIERANIE LOGINU OD ZALOGOWANEGO UZYTKOWNIKA
		 */
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();

		ModelMapper modelMapper = new ModelMapper();
		AlertDto alertDto = modelMapper.map(alert, AlertDto.class);
		alertDto.ustawCenterUserLogin(currentPrincipalName);
		AlertDto createdAlert = alertService.createAlert(alertDto);

		alertRest = modelMapper.map(createdAlert, AlertRest.class);
		return alertRest;
	}

	@ApiOperation(value = "Get Alert endpoint", notes = "${alertController.getAlert.apiDesc}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public AlertRest getAlert(@PathVariable String id) {

		AlertRest alertRest = new AlertRest();

		AlertDto alertDto = alertService.getAlertByAlertId(id);
		ModelMapper modelMapper = new ModelMapper();
		alertRest = modelMapper.map(alertDto, AlertRest.class);

		return alertRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Update Alert endpoint", notes = "${alertController.updateAlert.apiDesc}")
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public AlertRest updateAlert(@PathVariable String id, @RequestBody AlertRequestModel alert)
			throws NameNotFoundException {

		AlertRest alertRest = new AlertRest();

		ModelMapper modelMapper = new ModelMapper();
		AlertDto alertDto = modelMapper.map(alert, AlertDto.class);

		AlertDto updateAlert = alertService.updateAlert(id, alertDto);
		alertRest = modelMapper.map(updateAlert, AlertRest.class);

		return alertRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Delete Alert endpoint", notes = "${alertController.deleteAlert.apiDesc}")
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteAlert(@PathVariable String id) throws Exception {

		OperationStatusModel deletedAlert = new OperationStatusModel();
		deletedAlert.setNameOfOperation(RequestOperationName.DELETE.name());

		alertService.deleteAlert(id);

		deletedAlert.setResult(RequestOperationStatus.SUCCESS.name());

		return deletedAlert;

	}

	@ApiOperation(value = "Get list of all Alerts endpoint", notes = "${allertController.getAllAlerts.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<AlertRest> getAllAlerts(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<AlertRest> listOfAlertsRests = new ArrayList<>();

		List<AlertDto> listOfAlertsDto = alertService.getAlerts(numberOfPage, maxPages);

		for (AlertDto alertDto : listOfAlertsDto) {

			AlertRest alertModel = new AlertRest();
			ModelMapper modelMapper = new ModelMapper();
			alertModel = modelMapper.map(alertDto, AlertRest.class);
			listOfAlertsRests.add(alertModel);
		}

		return listOfAlertsRests;
	}

}
