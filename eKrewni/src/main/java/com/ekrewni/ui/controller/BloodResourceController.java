package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.service.AlertService;
import com.ekrewni.service.BloodResourceService;
import com.ekrewni.service.BloodService;
import com.ekrewni.service.CenterService;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.ui.model.request.BloodResourceRequestModel;
import com.ekrewni.ui.model.response.BloodResourceRest;
import com.ekrewni.ui.model.response.OperationStatusModel;
import com.ekrewni.ui.model.response.RequestOperationStatus;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("bloodresources") // http://localhost:8080/ekrewni/bloodresources
public class BloodResourceController {

	@Autowired
	CenterController centerController;

	@Autowired
	BloodResourceService bloodResourceService;

	@Autowired
	AlertService alertService;

	@Autowired
	BloodService bloodService;

	@Autowired
	CenterService centerService;

	@ApiOperation(value = "Get BloodResource endpoint", notes = "${bloodResourceController.getBloodResource.apiDesc}")
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public BloodResourceRest getBloodResource(@PathVariable String id) {

		BloodResourceRest bloodResourceRest = new BloodResourceRest();

		BloodResourceDto bloodResourceDto = bloodResourceService.getBloodResourceByBloodResourceId(id);
		ModelMapper modelMapper = new ModelMapper();
		bloodResourceRest = modelMapper.map(bloodResourceDto, BloodResourceRest.class);

		return bloodResourceRest;
	}

	@ApiImplicitParams({
	@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Create BloodResource endpoint", notes = "${bloodResourceController.createBloodResource.apiDesc}")
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public BloodResourceRest createBloodResource(@RequestBody BloodResourceRequestModel bloodResourceRequestModel) {

		BloodResourceRest bloodResourceRest = new BloodResourceRest();

		ModelMapper modelMapper = new ModelMapper();
		BloodResourceDto bloodResourceDto = modelMapper.map(bloodResourceRequestModel, BloodResourceDto.class);

		BloodResourceDto createdBloodResource = bloodResourceService.createBloodResource(bloodResourceDto);
		bloodResourceRest = modelMapper.map(createdBloodResource, BloodResourceRest.class);

		return bloodResourceRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Update BloodResource endpoint", notes = "${bloodResourceController.updateBloodResource.apiDesc}")
	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public BloodResourceRest updateBloodResource(@PathVariable String id,
			@RequestBody BloodResourceRequestModel bloodResourceRequestModel) throws NameNotFoundException {

		BloodResourceRest bloodResourceRest = new BloodResourceRest();

		ModelMapper modelMapper = new ModelMapper();
		BloodResourceDto bloodResourceDto = modelMapper.map(bloodResourceRequestModel, BloodResourceDto.class);

		BloodResourceDto updateBloodResource = bloodResourceService.updateBloodResource(id, bloodResourceDto);
		bloodResourceRest = modelMapper.map(updateBloodResource, BloodResourceRest.class);

		return bloodResourceRest;
	}

	@ApiOperation(value = "Delete BloodResource endpoint", notes = "${bloodResourceController.deleteBloodResource.apiDesc}")
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteBloodResource(@PathVariable String id) throws Exception {

		OperationStatusModel deletedBloodResource = new OperationStatusModel();
		deletedBloodResource.setNameOfOperation(RequestOperationName.DELETE.name());

		bloodResourceService.deleteBloodResource(id);

		deletedBloodResource.setResult(RequestOperationStatus.SUCCESS.name());
		return deletedBloodResource;
	}

	@ApiOperation(value = "Get list of all BloodResources endpoint", notes = "${bloodResourceController.getAllBloodResources.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<BloodResourceRest> getAllBloodResources(
			@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<BloodResourceRest> listOfBloodResourceRests = new ArrayList<>();

		List<BloodResourceDto> listOfBloodResourceDtos = bloodResourceService.getBloodResources(numberOfPage, maxPages);

		for (BloodResourceDto bloodResourceDto : listOfBloodResourceDtos) {

			BloodResourceRest boodResourceModel = new BloodResourceRest();

			ModelMapper modelMapper = new ModelMapper();
			boodResourceModel = modelMapper.map(bloodResourceDto, BloodResourceRest.class);
			listOfBloodResourceRests.add(boodResourceModel);
		}

		Comparator<BloodResourceRest> compareByCenterId = (BloodResourceRest b1, BloodResourceRest b2) -> b1
				.getCenterId().compareTo(b2.getCenterId());
		Collections.sort(listOfBloodResourceRests, compareByCenterId);
		return listOfBloodResourceRests;
	}

}
