package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.exceptions.CenterUserServiceException;
import com.ekrewni.service.CenterMessageService;
import com.ekrewni.shared.dto.CenterMessageDto;
import com.ekrewni.ui.model.request.CenterMessageRequestModel;
import com.ekrewni.ui.model.response.CenterMessageRest;
import com.ekrewni.ui.model.response.ErrorMessages;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("shoutbox")
public class ShoutboxController {

	@Autowired
	CenterMessageService centerMessageService;

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Get list of centers' messages endpoint", notes = "${centerUserController.getAllCenterUser.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<CenterMessageRest> getCentersMessages(
			@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<CenterMessageRest> listOfCentersMessagesRests = new ArrayList<>();

		List<CenterMessageDto> listOfCentersMessagesDto = centerMessageService.getCentersMessages(numberOfPage, maxPages);

		for (CenterMessageDto centerMessageDto : listOfCentersMessagesDto) {

			CenterMessageRest centerMessageModel = new CenterMessageRest();
			ModelMapper modelMapper = new ModelMapper();
			centerMessageModel = modelMapper.map(centerMessageDto, CenterMessageRest.class);
			listOfCentersMessagesRests.add(centerMessageModel);
		}

		return listOfCentersMessagesRests;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@ApiOperation(value = "Create CenterMessage endpoint", notes = "${centerMessageController.createCenterMessage.apiDesc}")
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public CenterMessageRest createCenterMessage(@RequestBody CenterMessageRequestModel centerMessageRequest)
			throws Exception {

		CenterMessageRest centerMessageRest = new CenterMessageRest();

		if (centerMessageRequest.getText().isEmpty()) {

			throw new CenterUserServiceException(ErrorMessages.REQUIRED_FIELD_IS_MISSING.getErrorMessage());
		}

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
	
		ModelMapper modelMapper = new ModelMapper();
		CenterMessageDto messageDto = modelMapper.map(centerMessageRequest, CenterMessageDto.class);

		messageDto.umiescCenterUserLogin(currentPrincipalName);
		CenterMessageDto createdCenterMessage = centerMessageService.createMessage(messageDto);
		centerMessageRest = modelMapper.map(createdCenterMessage, CenterMessageRest.class);

		return centerMessageRest;
	}
}
