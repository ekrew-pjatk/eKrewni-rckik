package com.ekrewni.ui.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.ui.model.request.ScreenNameRequestModel;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class VerificationController {

	@ApiOperation("CenterUser login")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Response Headers", responseHeaders = {
			@ResponseHeader(name = "authorization", description = "Holder <generated random token>", response = ScreenNameRequestModel.class),
			@ResponseHeader(name = "centerUserId", description = "Public CenterUser Id value here <centerUSerId>", response = ScreenNameRequestModel.class) }) })
			
	@PostMapping("/centerusers/login")
	public void login(@RequestBody ScreenNameRequestModel screenNameRequestModel) {

		throw new IllegalStateException("Don't call this method. Spring Security implements it.");
	}
}
