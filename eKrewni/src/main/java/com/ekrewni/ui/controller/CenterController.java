package com.ekrewni.ui.controller;

import java.util.ArrayList;
import java.util.List;

//import java.lang.reflect.Type;
//import java.util.ArrayList;
//import java.util.List;

import javax.naming.NameNotFoundException;

import org.modelmapper.ModelMapper;
//import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ekrewni.service.AddressService;
import com.ekrewni.service.AlertService;
import com.ekrewni.service.BloodResourceService;
import com.ekrewni.service.BloodService;
import com.ekrewni.service.CenterMessageService;
import com.ekrewni.service.CenterService;
import com.ekrewni.service.CenterUserService;
import com.ekrewni.shared.dto.AddressDto;
import com.ekrewni.shared.dto.AlertDto;
import com.ekrewni.shared.dto.BloodResourceDto;
import com.ekrewni.shared.dto.CenterDto;
import com.ekrewni.shared.dto.CenterUserDto;
//import com.ekrewni.shared.dto.CenterMessageDto;
import com.ekrewni.ui.model.request.AddressRequestModel;
import com.ekrewni.ui.model.request.CenterRequestModel;
import com.ekrewni.ui.model.response.AddressRest;
import com.ekrewni.ui.model.response.AlertRest;
import com.ekrewni.ui.model.response.BloodResourceRest;
import com.ekrewni.ui.model.response.BloodRest;
//import com.ekrewni.ui.model.response.CenterMessageRest;
import com.ekrewni.ui.model.response.CenterRest;
import com.ekrewni.ui.model.response.CenterWithBloodResourcesRest;
import com.ekrewni.ui.model.response.OperationStatusModel;
import com.ekrewni.ui.model.response.RequestOperationStatus;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("centers") // http://localhost:8080/ekrewni/centers
public class CenterController {

	@Autowired
	CenterService centerService;

	@Autowired
	BloodResourceService bloodResourceService;

	@Autowired
	BloodService bloodService;

	@Autowired
	AddressService addressService;

	@Autowired
	CenterMessageService centerMessageService;

	@Autowired
	CenterUserService centerUserService;

	@Autowired
	AlertService alertService;

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public CenterRest getCenter(@PathVariable String id) {

		CenterRest centerRest = new CenterRest();

		CenterDto centerDto = centerService.getCenterByCenterId(id);
		ModelMapper modelMapper = new ModelMapper();
		centerRest = modelMapper.map(centerDto, CenterRest.class);

		return centerRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public CenterRest createCenter(@RequestBody CenterRequestModel center) {

		CenterRest centerRest = new CenterRest();

		if (center.getName().isEmpty())
			throw new NullPointerException("The object is null");

		ModelMapper modelMapper = new ModelMapper();
		CenterDto centerDto = modelMapper.map(center, CenterDto.class);

		CenterDto createdCenter = centerService.createCenter(centerDto);
		centerRest = modelMapper.map(createdCenter, CenterRest.class);

		return centerRest;
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public CenterRest updateCenter(@PathVariable String id, @RequestBody CenterRequestModel center)
			throws NameNotFoundException {

		CenterRest centerRest = new CenterRest();

		ModelMapper modelMapper = new ModelMapper();
		CenterDto centerDto = modelMapper.map(center, CenterDto.class);

		CenterDto updateCenter = centerService.updateCenter(id, centerDto);
		centerRest = modelMapper.map(updateCenter, CenterRest.class);

		return centerRest;
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "authorization", value = "${centerUserController.authorizationHeader.description}", paramType = "header") })
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteCenter(@PathVariable String id) throws Exception {

		OperationStatusModel deletedCenter = new OperationStatusModel();
		deletedCenter.setNameOfOperation(RequestOperationName.DELETE.name());

		centerService.deleteCenter(id);

		deletedCenter.setResult(RequestOperationStatus.SUCCESS.name());
		return deletedCenter;
	}

	@ApiOperation(value = "Get list of Centers endpoint", notes = "${centerController.getAllCenters.apiDesc}")
	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<CenterRest> getAllCenters(@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<CenterRest> listOfCenterRests = new ArrayList<>();

		List<CenterDto> listOfCenterDtos = centerService.getCenters(numberOfPage, maxPages);

		for (CenterDto centerDto : listOfCenterDtos) {

			CenterRest centerModel = new CenterRest();
			ModelMapper modelMapper = new ModelMapper();
			centerModel = modelMapper.map(centerDto, CenterRest.class);
			listOfCenterRests.add(centerModel);
		}

		return listOfCenterRests;
	}

	@ApiOperation(value = "Get list of Centers with bloodresources endpoint", notes = "${centerController.getAllCentersWithBloodresources.apiDesc}")
	@GetMapping(path = "/centerswithresources", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<CenterWithBloodResourcesRest> getAllCentersWithBloodResources(
			@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<CenterWithBloodResourcesRest> listOfCenterWithResourcesRests = new ArrayList<>();

		List<CenterDto> listOfCenterDtos = centerService.getCenters(0, 10000);

		for (CenterDto centerDto : listOfCenterDtos) {

			CenterWithBloodResourcesRest centerModelRest = new CenterWithBloodResourcesRest();
			AddressRest addressRest = new AddressRest();

			addressRest.setAddressId(centerDto.getAddress().getAddressId());
			addressRest.setCity(centerDto.getAddress().getCity());
			addressRest.setStreet(centerDto.getAddress().getStreet());
			addressRest.setPostalCode(centerDto.getAddress().getPostalCode());
			addressRest.setLatitude(centerDto.getAddress().getLatitude());
			addressRest.setLongitude(centerDto.getAddress().getLongitude());
			
			centerModelRest.setAddress(addressRest);
			centerModelRest.setListOfBloodResources(new ArrayList<>());
			centerModelRest.setCenterId(centerDto.getCenterId());
			centerModelRest.setName(centerDto.getName());
			centerModelRest.setPhoneNumber(centerDto.getPhoneNumber());
			centerModelRest.setEmail(centerDto.getEmail());
			centerModelRest.setWebSite(centerDto.getWebSite());

			List<BloodResourceDto> bloodResourcesDto = centerDto.getBloodResources();
			List<BloodResourceRest> bloodResourcesRestList = new ArrayList<>();

			for (BloodResourceDto bloodResourceDto : bloodResourcesDto) {

				BloodResourceRest bloodResourceRest = new BloodResourceRest();
				bloodResourceRest.setBloodResourceId(bloodResourceDto.getBloodResourceId());
				bloodResourceRest.setVolume(bloodResourceDto.getVolume());
				
				BloodRest bloodRest = new BloodRest();
				bloodRest.setBloodId(bloodResourceDto.getBlood().getBloodId());
				bloodRest.setBloodType(bloodResourceDto.getBlood().getBloodType());
				bloodRest.setRh(bloodResourceDto.getBlood().getRh());
				
				bloodResourceRest.setBlood(bloodRest);
				bloodResourcesRestList.add(bloodResourceRest);
			centerModelRest.setListOfBloodResources(bloodResourcesRestList);

			listOfCenterWithResourcesRests.add(centerModelRest);
		}

	}
		return listOfCenterWithResourcesRests;}

	@GetMapping(path = "/{centerId}/bloodresources", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<BloodResourceRest> getBloodResourcesForGivenCenter(@PathVariable String centerId) {

		List<BloodResourceRest> listOfBloodResourcesRest = new ArrayList<>();

		List<BloodResourceDto> listOfBloodResourceDtos = bloodResourceService.getBloodResourceForGivenCenter(centerId);

		for (BloodResourceDto bloodResourceDto : listOfBloodResourceDtos) {

			BloodResourceRest bloodResourceModel = new BloodResourceRest();
			ModelMapper modelMapper = new ModelMapper();
			bloodResourceModel = modelMapper.map(bloodResourceDto, BloodResourceRest.class);
			listOfBloodResourcesRest.add(bloodResourceModel);

		}

		return listOfBloodResourcesRest;
	}

	@GetMapping(path = "/{centerId}/alerts", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public List<AlertRest> getAlertsForGivenCenter(@PathVariable String centerId,
			@RequestParam(value = "page", defaultValue = "0") int numberOfPage,
			@RequestParam(value = "limit", defaultValue = "25") int maxPages) {

		List<AlertRest> listOfAlertRest = new ArrayList<>();

		CenterUserDto centerUserDto = centerUserService.getCenterUserByCenterId(centerId);
		String centerUserId = centerUserDto.getUserId();

		List<AlertDto> listOfAlertDtos = alertService.getAlertsByUserId(centerUserId, numberOfPage, maxPages);

		for (AlertDto alertDto : listOfAlertDtos) {

			AlertRest alertRest = new AlertRest();
			ModelMapper modelMapper = new ModelMapper();
			alertRest = modelMapper.map(alertDto, AlertRest.class);
			listOfAlertRest.add(alertRest);

		}

		return listOfAlertRest;
	}

	@GetMapping(path = "/{centerId}/addresses", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public AddressRest getCenterAddress(@PathVariable String centerId) throws NameNotFoundException {

		AddressDto addressesDto = addressService.getAddressByCenterId(centerId);

		ModelMapper modelMapper = new ModelMapper();

		return modelMapper.map(addressesDto, AddressRest.class);
	}

	@PutMapping(path = "/{centerId}/addresses/{addressId}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public AddressRest updateCentersAddress(@PathVariable String addressId, @RequestBody AddressRequestModel address)
			throws NameNotFoundException {

		AddressRest addressRest = new AddressRest();

		ModelMapper modelMapper = new ModelMapper();
		AddressDto addressDto = modelMapper.map(address, AddressDto.class);

		AddressDto updateAddress = addressService.updateAddress(addressId, addressDto);
		addressRest = modelMapper.map(updateAddress, AddressRest.class);

		return addressRest;
	}

}
