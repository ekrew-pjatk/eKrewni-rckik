package com.ekrewni.ui.model.request;

public class CenterRequestModel {

	private String name;
	private String phoneNumber;
	private String email;
	private String webSite;
	private AddressRequestModel address;

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getPhoneNumber() {

		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getWebSite() {

		return webSite;
	}

	public void setWebSite(String webSite) {

		this.webSite = webSite;
	}

	public AddressRequestModel getAddress() {

		return address;
	}

	public void setAddress(AddressRequestModel address) {

		this.address = address;
	}

}
