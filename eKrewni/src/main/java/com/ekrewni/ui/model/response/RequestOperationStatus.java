package com.ekrewni.ui.model.response;

public enum RequestOperationStatus {

	ERROR, SUCCESS
}
