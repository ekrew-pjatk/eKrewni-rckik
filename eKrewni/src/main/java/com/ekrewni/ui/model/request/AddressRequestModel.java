package com.ekrewni.ui.model.request;

public class AddressRequestModel {

	private String city;
	private String postalCode;
	private String street;
    private String latitude;
	private String longitude;

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getPostalCode() {

		return postalCode;
	}

	public void setPostalCode(String postalCode) {

		this.postalCode = postalCode;
	}

	public String getStreet() {

		return street;
	}

	public void setStreet(String street) {

		this.street = street;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
