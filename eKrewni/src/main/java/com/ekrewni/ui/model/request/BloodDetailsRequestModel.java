package com.ekrewni.ui.model.request;

public class BloodDetailsRequestModel {

	private String bloodType;
	private String rh;

	public String getBloodType() {

		return bloodType;
	}

	public void setBloodType(String bloodType) {

		this.bloodType = bloodType;
	}

	public String getRh() {

		return rh;
	}

	public void setRh(String rh) {

		this.rh = rh;
	}

}
