package com.ekrewni.ui.model.request;



public class CenterUserWithCenterIdRequestModel {

		private String login;
		private String password;
		private String centerId;

		public String getLogin() {

			return login;
		}

		public void setLogin(String login) {

			this.login = login;
		}

		public String getPassword() {

			return password;

		}

		public void setPassword(String password) {

			this.password = password;

		}

		public String getCenterId() {
			return centerId;
		}
	
		public void setCenterId(String centerId) {
			this.centerId = centerId;
		}
	}
