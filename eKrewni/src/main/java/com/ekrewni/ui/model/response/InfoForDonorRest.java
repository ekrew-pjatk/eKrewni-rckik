package com.ekrewni.ui.model.response;

public class InfoForDonorRest {

	private String info;

	public String getInfo() {

		return info;
	}

	public void setInfo(String info) {

		this.info = info;
	}

}
