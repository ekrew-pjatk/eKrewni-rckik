package com.ekrewni.ui.model.response;

import java.time.LocalDateTime;

public class SearchAlertRest {
	private String alertId;
	private String message;
	private LocalDateTime date;
	private BloodRest blood;
	private String centerId;

	public String getAlertId() {
		return alertId;
	}

	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public BloodRest getBlood() {
		return blood;
	}

	public void setBlood(BloodRest blood) {
		this.blood = blood;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}
	
}
