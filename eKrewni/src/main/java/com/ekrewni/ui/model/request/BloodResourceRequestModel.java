package com.ekrewni.ui.model.request;

public class BloodResourceRequestModel {

	private double volume;

	public double getVolume() {

		return volume;
	}
	
	public void setVolume(double volume) {

		this.volume = volume;
	}
	
}
