package com.ekrewni.ui.model.request;

public class AlertRequestModel {

	private String message;
	private BloodDetailsRequestModel bloodDetailsRequestModel;

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = message;
	}

	public BloodDetailsRequestModel getBloodDetailsRequestModel() {

		return bloodDetailsRequestModel;
	}

	public void setBloodDetailsRequestModel(BloodDetailsRequestModel bloodDetailsRequestModel) {

		this.bloodDetailsRequestModel = bloodDetailsRequestModel;
	}
}