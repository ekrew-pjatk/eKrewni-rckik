package com.ekrewni.ui.model.response;

public enum ErrorMessages {

	REQUIRED_FIELD_IS_MISSING("Missing required field. Please chceck documentation for required fields."),
	RECORD_ALREADY_EXISTS("Record already exists"), 
	INTERNAL_SERVER_ERROR("Internal server error"),
	NO_RECORD_FOUND("Record with provided id is not found"), 
	AUTHENTICATION_FAILED("Authentication failed"),
	COULD_NOT_UPDATE_RECORD("Could not update record"), 
	COULD_NOT_DELETE_RECORD("Could not delete record");

	private String errorMessage;

	private ErrorMessages(String errorMessage) {

		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}
	
}
