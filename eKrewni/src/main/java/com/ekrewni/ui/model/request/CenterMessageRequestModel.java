package com.ekrewni.ui.model.request;

public class CenterMessageRequestModel {

	private String text;

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}
}
