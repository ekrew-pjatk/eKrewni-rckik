package com.ekrewni.ui.model.response;

import java.util.List;

public class CenterWithBloodResourcesRest {
	
	private String centerId;
	private String name;
	private String phoneNumber;
	private String email;
	private String webSite;
	private AddressRest address;
	private List<BloodResourceRest> listOfBloodResources;
	private CenterUserRest centerUser;
	
	public String getCenterId() {
	
		return centerId;
	}
	
	public void setCenterId(String centerId) {
	
		this.centerId = centerId;
	}
	
	public String getName() {
	
		return name;
	}
	
	public void setName(String name) {
	
		this.name = name;
	}
	
	public String getPhoneNumber() {
	
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
	
		this.phoneNumber = phoneNumber;
	}
	
	public String getEmail() {
	
		return email;
	}
	
	public void setEmail(String email) {
	
		this.email = email;
	}
	
	public String getWebSite() {
	
		return webSite;
	}
	
	public void setWebSite(String webSite) {
	
		this.webSite = webSite;
	}
	
	public AddressRest getAddress() {
	
		return address;
	}
	
	public void setAddress(AddressRest address) {
	
		this.address = address;
	}
	
	public List<BloodResourceRest> getListOfBloodResources() {
	
		return listOfBloodResources;
	}
	
	public void setListOfBloodResources(List<BloodResourceRest> listOfBloodResources) {
	
		this.listOfBloodResources = listOfBloodResources;
	}

	public CenterUserRest getCenterUser() {

		return null;
	}

	public void setCenterUser(CenterUserRest centerUser) {

		this.centerUser = centerUser;
	}

}
