package com.ekrewni.ui.model.response;

public class BloodRest {

	private String bloodId;
	private String bloodType;
	private String rh;

	public String getBloodId() {

		return bloodId;
	}

	public void setBloodId(String bloodId) {

		this.bloodId = bloodId;
	}

	public String getBloodType() {

		return bloodType;
	}

	public void setBloodType(String bloodType) {

		this.bloodType = bloodType;
	}

	public String getRh() {

		return rh;
	}

	public void setRh(String rh) {

		this.rh = rh;
	}
	
}
