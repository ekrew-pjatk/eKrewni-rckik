package com.ekrewni.ui.model.response;

import java.time.LocalDateTime;

public class CenterMessageRest {

	private String centerMessageId;
	private String text;
	private LocalDateTime dateTime;

	public String getCenterMessageId() {

		return centerMessageId;
	}

	public void setCenterMessageId(String centerMessageId) {

		this.centerMessageId = centerMessageId;
	}

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}

	public LocalDateTime getDateTime() {

		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {

		this.dateTime = dateTime;
	}
}
