package com.ekrewni.ui.model.response;

public class CenterUserRest {

	private String userId;
	private String login;
	private String centerId;

	public String getUserId() {

		return userId;
	}

	public void setUserId(String userId) {

		this.userId = userId;
	}

	public String getLogin() {

		return login;
	}

	public void setLogin(String login) {

		this.login = login;
	}

	public String getCenterId() {

		return centerId;
	}

	public void setCenterId(String centerId) {

		this.centerId = centerId;
	}
	
}
