package com.ekrewni.ui.model.response;

public class BloodResourceRest {

	private String bloodResourceId;
	private double volume;
	private BloodRest blood;
	private String centerId;

	public String getBloodResourceId() {

		return bloodResourceId;
	}

	public void setBloodResourceId(String bloodResourceId) {

		this.bloodResourceId = bloodResourceId;
	}

	public double getVolume() {

		return volume;
	}

	public void setVolume(double volume) {

		this.volume = volume;
	}

	public BloodRest getBlood() {

		return blood;
	}

	public void setBlood(BloodRest blood) {

		this.blood = blood;
	}

	public String getCenterId() {

		return centerId;
	}

	public void setCenterId(String centerId) {

		this.centerId = centerId;
	}
	
}
