package com.ekrewni.ui.model.response;

public class OperationStatusModel {

	private String result;
	private String nameOfOperation;

	public String getResult() {

		return result;
	}

	public void setResult(String result) {

		this.result = result;
	}

	public String getNameOfOperation() {

		return nameOfOperation;
	}

	public void setNameOfOperation(String nameOfOperation) {

		this.nameOfOperation = nameOfOperation;
	}
	
}
